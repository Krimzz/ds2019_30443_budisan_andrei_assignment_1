package edu.ro.utcn.assignment1.repositories;

import edu.ro.utcn.assignment1.entities.SideEffect;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SideEffectRepository extends JpaRepository<SideEffect, Integer> {

}
