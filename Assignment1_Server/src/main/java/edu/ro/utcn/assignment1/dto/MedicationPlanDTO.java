package edu.ro.utcn.assignment1.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import edu.ro.utcn.assignment1.entities.Drug;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MedicationPlanDTO {

    private Integer id;
    private String startDate;
    private String endDate;
    private List<Drug> drugs;

    @JsonCreator
    public MedicationPlanDTO(@JsonProperty("id") Integer id,
                             @JsonProperty("startDate") String startDate,
                             @JsonProperty("endDate") String endDate,
                             @JsonProperty("drugs") List<Drug> drugs) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.drugs = drugs;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("startDate")
    public String getStartDate() {
        return startDate;
    }

    @JsonProperty("endDate")
    public String getEndDate() {
        return endDate;
    }

    @JsonProperty("drugs")
    public List<Drug> getDrugs() {
        return drugs;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MedicationPlanDTO{");
        sb.append("id='").append(getId()).append('\'');
        sb.append(", startDate='").append(getStartDate()).append('\'');
        sb.append(", endDate='").append(getEndDate()).append('\'');
        sb.append(", drugs=").append(getDrugs());
        sb.append('}');
        return sb.toString();
    }
}
