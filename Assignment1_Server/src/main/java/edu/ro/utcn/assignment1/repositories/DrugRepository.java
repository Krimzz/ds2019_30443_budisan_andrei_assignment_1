package edu.ro.utcn.assignment1.repositories;

import edu.ro.utcn.assignment1.entities.Drug;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DrugRepository extends JpaRepository<Drug, Integer> {
}
