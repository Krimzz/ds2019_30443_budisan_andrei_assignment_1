package edu.ro.utcn.assignment1.dto.converters;

import edu.ro.utcn.assignment1.dto.CaregiverDTO;
import edu.ro.utcn.assignment1.entities.Caregiver;

import java.util.ArrayList;
import java.util.List;

public class CaregiverConverter {

    private CaregiverConverter() {

    }

    public static CaregiverDTO toDto(Caregiver model) {
        CaregiverDTO dto = null;
        if (model != null) {
            dto = new CaregiverDTO(model.getUsername(),
                    model.getPassword(),
                    model.getName(),
                    model.getAddress(),
                    model.getGender(),
                    model.getBirthDate(),
                    model.getPatients());
        }
        return dto;
    }

    public static List<CaregiverDTO> toDto(List<Caregiver> models) {
        List<CaregiverDTO> caregiverDTOS = new ArrayList<>();
        if ((models != null) && (!models.isEmpty())) {
            for (Caregiver caregiver : models) {
                caregiverDTOS.add(toDto(caregiver));
            }
        }
        return caregiverDTOS;
    }
}
