package edu.ro.utcn.assignment1.services;

import edu.ro.utcn.assignment1.dto.DrugDTO;
import edu.ro.utcn.assignment1.dto.converters.DrugConverter;
import edu.ro.utcn.assignment1.entities.Drug;
import edu.ro.utcn.assignment1.repositories.DrugRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DrugService {

    @Autowired
    private DrugRepository drugRepository;

    public List<DrugDTO> getAllDrugs(){
        return DrugConverter.toDto(drugRepository.findAll());
    }

    public DrugDTO getDrugById(Integer id) throws Exception {
        Drug drug = drugRepository.findById(id).orElse(null);
        if (drug == null) throw new NotFoundException("No drug found with that id");
        return DrugConverter.toDto(drug);
    }

    public DrugDTO createDrug(DrugDTO drugDTO) throws Exception {
        Drug drug = new Drug(drugDTO.getId(),
                drugDTO.getName(),
                drugDTO.getIntakeStartTime(),
                drugDTO.getIntakeEndTime(),
                drugDTO.getDosage(),
                drugDTO.getSideEffects());
        Drug newDrug = drugRepository.save(drug);
        return DrugConverter.toDto(newDrug);
    }

    public DrugDTO updateDrug(DrugDTO drugDTO) throws Exception {
        Drug drug = drugRepository.findById(drugDTO.getId()).orElse(null);
        if (drug == null) {
            throw new NotFoundException("No drug found with that id");
        }

        if(!drugDTO.getName().equals(drug.getName())) {
            drug.setName(drugDTO.getName());
        }

        if(!drugDTO.getIntakeStartTime().equals(drug.getIntakeStartTime())) {
            drug.setIntakeStartTime(drugDTO.getIntakeStartTime());
        }

        if(!drugDTO.getIntakeEndTime().equals(drug.getIntakeEndTime())) {
            drug.setIntakeEndTime(drugDTO.getIntakeEndTime());
        }

        if(!drugDTO.getSideEffects().equals(drug.getSideEffects())) {
            drug.setSideEffects(drugDTO.getSideEffects());
        }

        return DrugConverter.toDto(drugRepository.save(drug));
    }

    public void deleteDrug(Integer id) throws Exception {
        Drug drug = drugRepository.findById(id).orElse(null);
        if (drug == null) {
            throw new NotFoundException("No drug with that id");
        }
        drugRepository.delete(drug);
    }
}
