package edu.ro.utcn.assignment1.dto.converters;

import edu.ro.utcn.assignment1.dto.PatientDTO;
import edu.ro.utcn.assignment1.entities.Patient;

import java.util.ArrayList;
import java.util.List;

public class PatientConverter {

    private PatientConverter() {

    }

    public static PatientDTO toDto(Patient model) {
        PatientDTO dto = null;
        if (model != null) {
            dto = new PatientDTO(model.getUsername(),
                    model.getPassword(),
                    model.getName(),
                    model.getAddress(),
                    model.getGender(),
                    model.getBirthDate(),
                    model.getMedicationPlans());
        }
        return dto;
    }

    public static List<PatientDTO> toDto(List<Patient> models) {
        List<PatientDTO> patientDTOS = new ArrayList<>();
        if ((models != null) && (!models.isEmpty())) {
            for (Patient patient : models) {
                patientDTOS.add(toDto(patient));
            }
        }
        return patientDTOS;
    }
}
