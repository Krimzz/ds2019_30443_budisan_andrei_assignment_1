package edu.ro.utcn.assignment1.controller;

import edu.ro.utcn.assignment1.dto.DoctorDTO;
import edu.ro.utcn.assignment1.services.DoctorService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("medicalplatform/doctor")
@CrossOrigin
public class DoctorController {

    @Autowired
    private DoctorService doctorService;

    @GetMapping
    public List<DoctorDTO> getAllDoctors() {
        return doctorService.getAllDoctors();
    }

    @GetMapping(value = "/{username}")
    public ResponseEntity<?> findDoctorByUsername(@PathVariable("username") String username) {
        try {
            DoctorDTO doctorDTO = doctorService.getDoctorByUsername(username);
            return new ResponseEntity<>(doctorDTO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<?> insertDoctor(@RequestBody DoctorDTO doctorDTO) {
        DoctorDTO doctorDTOtoBeInserted;
        try {
            doctorDTOtoBeInserted = doctorService.createDoctor(doctorDTO);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(doctorDTOtoBeInserted, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<?> updateDoctor(@RequestBody DoctorDTO doctorDTO) {
        try {
            DoctorDTO changedDoctor = doctorService.updateDoctor(doctorDTO);
            return new ResponseEntity<>(changedDoctor, HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value = "/{username}")
    public ResponseEntity<?> deleteDoctor(@PathVariable("username") String username) {
        try {
            doctorService.deleteDoctor(username);
            return new ResponseEntity<String>(HttpStatus.OK);
        } catch (NotFoundException ne) {
            return new ResponseEntity<>(ne.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
