package edu.ro.utcn.assignment1.entities;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@PrimaryKeyJoinColumn(name = "username")
@Table(name = "caregiver")
public class Caregiver extends User {

    @OneToMany
    private Set<Patient> patients;

    public Caregiver() {

    }

    public Caregiver(String username,
                     String password,
                     String name,
                     String address,
                     String gender,
                     String birthDate,
                     Set<Patient> patients) {
        super(username, password, name, Caregiver.class.getSimpleName(), address, gender, birthDate);
        this.patients = patients;
    }

    public Set<Patient> getPatients() {
        return patients;
    }

    public void setPatients(Set<Patient> patients) {
        this.patients = patients;
    }
}
