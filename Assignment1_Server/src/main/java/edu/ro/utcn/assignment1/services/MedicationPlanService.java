package edu.ro.utcn.assignment1.services;

import edu.ro.utcn.assignment1.dto.MedicationPlanDTO;
import edu.ro.utcn.assignment1.dto.converters.MedicationPlanConverter;
import edu.ro.utcn.assignment1.entities.Doctor;
import edu.ro.utcn.assignment1.entities.MedicationPlan;
import edu.ro.utcn.assignment1.entities.Patient;
import edu.ro.utcn.assignment1.repositories.DoctorRepository;
import edu.ro.utcn.assignment1.repositories.MedicationPlanRepository;
import edu.ro.utcn.assignment1.repositories.PatientRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicationPlanService {

    @Autowired
    private MedicationPlanRepository medicationPlanRepository;

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private PatientRepository patientRepository;

    public List<MedicationPlanDTO> getAllMedicationPlans(){
        return MedicationPlanConverter.toDto(medicationPlanRepository.findAll());
    }

    public MedicationPlanDTO getMedicationPlanById(Integer id) throws Exception {
        MedicationPlan medicationPlan = medicationPlanRepository.findById(id).orElse(null);
        if (medicationPlan == null) throw new NotFoundException("No medication plan found with that id");
        return MedicationPlanConverter.toDto(medicationPlan);
    }

    public MedicationPlanDTO createMedicationPlan(MedicationPlanDTO medicationPlanDTO) throws Exception {
        MedicationPlan medicationPlan = new MedicationPlan(medicationPlanDTO.getId(),
                medicationPlanDTO.getStartDate(),
                medicationPlanDTO.getEndDate(),
                medicationPlanDTO.getDrugs());
        MedicationPlan newMedicationPlan = medicationPlanRepository.save(medicationPlan);
        return MedicationPlanConverter.toDto(newMedicationPlan);
    }

    public MedicationPlanDTO updateMedicationPlan(MedicationPlanDTO medicationPlanDTO) throws Exception {
        MedicationPlan medicationPlan = medicationPlanRepository.findById(medicationPlanDTO.getId()).orElse(null);
        if (medicationPlan == null) {
            throw new NotFoundException("No medication plan found with that id");
        }

        if(!medicationPlanDTO.getStartDate().equals(medicationPlan.getStartDate())) {
            medicationPlan.setStartDate(medicationPlanDTO.getStartDate());
        }

        if(!medicationPlanDTO.getEndDate().equals(medicationPlan.getEndDate())) {
            medicationPlan.setEndDate(medicationPlanDTO.getEndDate());
        }
        if(!medicationPlanDTO.getDrugs().equals(medicationPlan.getDrugs())) {
            medicationPlan.setDrugs(medicationPlanDTO.getDrugs());
        }

        return MedicationPlanConverter.toDto(medicationPlanRepository.save(medicationPlan));
    }

    public void deleteMedicationPlan(Integer id) throws Exception {
        MedicationPlan medicationPlan = medicationPlanRepository.findById(id).orElse(null);
        if (medicationPlan == null) {
            throw new NotFoundException("No medication plan with that id");
        }

        List<Patient> patients = patientRepository.findAll();

        for(Patient patient: patients) {
            patient.getMedicationPlans().remove(medicationPlan);
            patientRepository.save(patient);
        }

        List<Doctor> doctors = doctorRepository.findAll();

        for(Doctor doctor: doctors) {
            doctor.getMedicationPlans().remove(medicationPlan);
            doctorRepository.save(doctor);
        }

        medicationPlanRepository.delete(medicationPlan);
    }

    public MedicationPlanDTO addMedicationPlanToPatient(String doctorUsername, String patientUsername, MedicationPlanDTO medicationPlanDTO) throws Exception {
        Doctor doctor = doctorRepository.findById(doctorUsername).orElse(null);
        if (doctor == null) throw new NotFoundException("No doctor found with that username");

        Patient patient = patientRepository.findById(patientUsername).orElse(null);
        if (patient == null) throw new NotFoundException("No patient found with that username");

        MedicationPlan medicationPlan = new MedicationPlan(medicationPlanDTO.getId(),
                medicationPlanDTO.getStartDate(),
                medicationPlanDTO.getEndDate(),
                medicationPlanDTO.getDrugs());
        MedicationPlan newMedicationPlan = medicationPlanRepository.save(medicationPlan);

        doctor.getMedicationPlans().add(medicationPlan);
        patient.getMedicationPlans().add(medicationPlan);

        doctorRepository.save(doctor);
        patientRepository.save(patient);

        return MedicationPlanConverter.toDto(newMedicationPlan);
    }
}
