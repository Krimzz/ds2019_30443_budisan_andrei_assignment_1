package edu.ro.utcn.assignment1.dto.converters;

import edu.ro.utcn.assignment1.dto.SideEffectDTO;
import edu.ro.utcn.assignment1.entities.SideEffect;

import java.util.ArrayList;
import java.util.List;

public class SideEffectConverter {
    
    private SideEffectConverter() {

    }

    public static SideEffectDTO toDto(SideEffect model) {
        SideEffectDTO dto = null;
        if (model != null) {
            dto = new SideEffectDTO(model.getId(),
                    model.getDescription());
        }
        return dto;
    }

    public static List<SideEffectDTO> toDto(List<SideEffect> models) {
        List<SideEffectDTO> sideEffectDTOS = new ArrayList<>();
        if ((models != null) && (!models.isEmpty())) {
            for (SideEffect sideEffect : models) {
                sideEffectDTOS.add(toDto(sideEffect));
            }
        }
        return sideEffectDTOS;
    }
}
