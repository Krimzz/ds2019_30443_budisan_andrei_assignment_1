package edu.ro.utcn.assignment1.controller;

import edu.ro.utcn.assignment1.dto.MedicationPlanDTO;
import edu.ro.utcn.assignment1.services.MedicationPlanService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("medicalplatform/doctor/medicationplan")
@CrossOrigin
public class MedicationPlanController {

    @Autowired
    private MedicationPlanService medicationPlanService;

    @GetMapping
    public List<MedicationPlanDTO> getAllMedicationPlans() {
        return medicationPlanService.getAllMedicationPlans();
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> findMedicationPlanById(@PathVariable("id") Integer id) {
        try {
            MedicationPlanDTO medicationPlanDTO = medicationPlanService.getMedicationPlanById(id);
            return new ResponseEntity<>(medicationPlanDTO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<?> insertMedicationPlan(@RequestBody MedicationPlanDTO medicationPlanDTO) {
        MedicationPlanDTO medicationPlanDTOtoBeInserted;
        try {
            medicationPlanDTOtoBeInserted = medicationPlanService.createMedicationPlan(medicationPlanDTO);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(medicationPlanDTOtoBeInserted, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<?> updateMedicationPlan(@RequestBody MedicationPlanDTO medicationPlanDTO) {
        try {
            MedicationPlanDTO changedMedicationPlan = medicationPlanService.updateMedicationPlan(medicationPlanDTO);
            return new ResponseEntity<>(changedMedicationPlan, HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deleteMedicationPlan(@PathVariable("id") Integer id) {
        try {
            medicationPlanService.deleteMedicationPlan(id);
            return new ResponseEntity<String>(HttpStatus.OK);
        } catch (NotFoundException ne) {
            return new ResponseEntity<>(ne.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/{doctorUsername}/{patientUsername}")
    public ResponseEntity<?> addMedicationPlanToPatient(@PathVariable("doctorUsername") String doctorUsername,
                                                  @PathVariable("patientUsername") String patientUsername,
                                                  @RequestBody MedicationPlanDTO medicationPlanDTO) {
        MedicationPlanDTO medicationPlanDTOtoBeInserted;
        try {
            medicationPlanDTOtoBeInserted = medicationPlanService.addMedicationPlanToPatient(doctorUsername, patientUsername, medicationPlanDTO);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(medicationPlanDTOtoBeInserted, HttpStatus.CREATED);
    }
}
