package edu.ro.utcn.assignment1.services;

import edu.ro.utcn.assignment1.dto.DoctorDTO;
import edu.ro.utcn.assignment1.dto.converters.DoctorConverter;
import edu.ro.utcn.assignment1.entities.Doctor;
import edu.ro.utcn.assignment1.entities.MedicationPlan;
import edu.ro.utcn.assignment1.entities.Patient;
import edu.ro.utcn.assignment1.repositories.DoctorRepository;
import edu.ro.utcn.assignment1.repositories.MedicationPlanRepository;
import edu.ro.utcn.assignment1.repositories.PatientRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DoctorService {

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private MedicationPlanRepository medicationPlanRepository;

    @Autowired
    private PatientRepository patientRepository;

    public List<DoctorDTO> getAllDoctors(){
        return DoctorConverter.toDto(doctorRepository.findAll());
    }

    public DoctorDTO getDoctorByUsername(String username) throws Exception {
        Doctor doctor = doctorRepository.findById(username).orElse(null);
        if (doctor == null) throw new NotFoundException("No doctor found with that username");
        return DoctorConverter.toDto(doctor);
    }

    public DoctorDTO createDoctor(DoctorDTO doctorDTO) throws Exception {
        Doctor doctor = new Doctor(doctorDTO.getUsername(),
                getEncodedPassword(doctorDTO.getPassword()),
                doctorDTO.getName(),
                doctorDTO.getAddress(),
                doctorDTO.getGender(),
                doctorDTO.getBirthDate(),
                doctorDTO.getDomain(),
                doctorDTO.getMedicationPlans());
        Doctor newDoctor = doctorRepository.save(doctor);
        return DoctorConverter.toDto(newDoctor);
    }

    public DoctorDTO updateDoctor(DoctorDTO doctorDTO) throws Exception {
        Doctor doctor = doctorRepository.findById(doctorDTO.getUsername()).orElse(null);
        if (doctor == null) {
            throw new NotFoundException("No doctor found with that username");
        }

        if(!doctorDTO.getUsername().equals(doctor.getUsername())) {
            doctor.setUsername(doctorDTO.getUsername());
        }

        if(!doctorDTO.getPassword().equals(doctor.getPassword())) {
            doctor.setPassword(getEncodedPassword(doctorDTO.getPassword()));
        }

        if(!doctorDTO.getName().equals(doctor.getName())) {
            doctor.setName(doctorDTO.getName());
        }

        if(!doctorDTO.getAddress().equals(doctor.getAddress())) {
            doctor.setAddress(doctorDTO.getAddress());
        }

        if(!doctorDTO.getGender().equals(doctor.getGender())) {
            doctor.setGender(doctorDTO.getGender());
        }

        if(!doctorDTO.getBirthDate().equals(doctor.getBirthDate())) {
            doctor.setBirthDate(doctorDTO.getBirthDate());
        }

        if(!doctorDTO.getDomain().equals(doctor.getDomain())) {
            doctor.setDomain(doctorDTO.getDomain());
        }

        if(!doctorDTO.getMedicationPlans().equals(doctor.getMedicationPlans())) {
            doctor.setMedicationPlans(doctorDTO.getMedicationPlans());
        }

        return DoctorConverter.toDto(doctorRepository.save(doctor));
    }

    public String getEncodedPassword(String password) {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(password);
    }

    public void deleteDoctor(String username) throws Exception {
        Doctor doctor = doctorRepository.findById(username).orElse(null);
        if (doctor == null) {
            throw new NotFoundException("No doctor with that username");
        }

        List<Patient> patients = patientRepository.findAll();

        for (Patient patient: patients) {
            for (MedicationPlan medicationPlan: doctor.getMedicationPlans()) {
                patient.getMedicationPlans().remove(medicationPlan);
            }
            patientRepository.save(patient);
        }

        doctorRepository.delete(doctor);
    }
}
