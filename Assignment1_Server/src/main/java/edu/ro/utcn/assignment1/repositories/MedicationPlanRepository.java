package edu.ro.utcn.assignment1.repositories;

import edu.ro.utcn.assignment1.entities.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, Integer> {

}
