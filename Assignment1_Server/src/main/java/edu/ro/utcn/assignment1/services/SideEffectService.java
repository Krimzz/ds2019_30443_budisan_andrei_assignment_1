package edu.ro.utcn.assignment1.services;

import edu.ro.utcn.assignment1.dto.SideEffectDTO;
import edu.ro.utcn.assignment1.dto.converters.SideEffectConverter;
import edu.ro.utcn.assignment1.entities.SideEffect;
import edu.ro.utcn.assignment1.repositories.SideEffectRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SideEffectService {

    @Autowired
    private SideEffectRepository sideEffectRepository;

    public List<SideEffectDTO> getAllSideEffects(){
        return SideEffectConverter.toDto(sideEffectRepository.findAll());
    }

    public SideEffectDTO getSideEffectById(Integer id) throws Exception {
        SideEffect sideEffect = sideEffectRepository.findById(id).orElse(null);
        if (sideEffect == null) throw new NotFoundException("No side effect found with that id");
        return SideEffectConverter.toDto(sideEffect);
    }

    public SideEffectDTO createSideEffect(SideEffectDTO sideEffectDTO) throws Exception {
        SideEffect sideEffect = new SideEffect(sideEffectDTO.getId(),
                sideEffectDTO.getDescription());
        SideEffect newSideEffect = sideEffectRepository.save(sideEffect);
        return SideEffectConverter.toDto(newSideEffect);
    }

    public SideEffectDTO updateSideEffect(SideEffectDTO sideEffectDTO) throws Exception {
        SideEffect sideEffect = sideEffectRepository.findById(sideEffectDTO.getId()).orElse(null);
        if (sideEffect == null) {
            throw new NotFoundException("No side effect found with that id");
        }

        if(!sideEffectDTO.getDescription().equals(sideEffect.getDescription())) {
            sideEffect.setDescription(sideEffectDTO.getDescription());
        }

        return SideEffectConverter.toDto(sideEffectRepository.save(sideEffect));
    }

    public void deleteSideEffect(Integer id) throws Exception {
        SideEffect sideEffect = sideEffectRepository.findById(id).orElse(null);
        if (sideEffect == null) {
            throw new NotFoundException("No side effect with that id");
        }
        sideEffectRepository.delete(sideEffect);
    }
}
