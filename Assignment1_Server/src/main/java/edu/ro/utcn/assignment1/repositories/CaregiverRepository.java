package edu.ro.utcn.assignment1.repositories;

import edu.ro.utcn.assignment1.entities.Caregiver;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CaregiverRepository extends JpaRepository<Caregiver, String> {

}
