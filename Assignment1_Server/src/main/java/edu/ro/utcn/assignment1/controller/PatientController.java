package edu.ro.utcn.assignment1.controller;

import edu.ro.utcn.assignment1.dto.PatientDTO;
import edu.ro.utcn.assignment1.services.PatientService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("medicalplatform")
@CrossOrigin
public class PatientController {

    @Autowired
    private PatientService patientService;

    @GetMapping(value = "/doctor/patient")
    public List<PatientDTO> getAllPatients() {
        return patientService.getAllPatients();
    }

    @GetMapping(value = {"/doctor/patient/{username}" , "/patient/{username}"})
    public ResponseEntity<?> findPatientByUsername(@PathVariable("username") String username) {
        try {
            PatientDTO patientDTO = patientService.getPatientByUsername(username);
            return new ResponseEntity<>(patientDTO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/doctor/patient")
    public ResponseEntity<?> insertPatient(@RequestBody PatientDTO patientDTO) {
        PatientDTO patientDTOtoBeInserted;
        try {
            patientDTOtoBeInserted = patientService.createPatient(patientDTO);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(patientDTOtoBeInserted, HttpStatus.CREATED);
    }

    @PutMapping(value = "/doctor/patient")
    public ResponseEntity<?> updatePatient(@RequestBody PatientDTO patientDTO) {
        try {
            PatientDTO changedPatient = patientService.updatePatient(patientDTO);
            return new ResponseEntity<>(changedPatient, HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value = "/doctor/patient/{username}")
    public ResponseEntity<?> deletePatient(@PathVariable("username") String username) {
        try {
            patientService.deletePatient(username);
            return new ResponseEntity<String>(HttpStatus.OK);
        } catch (NotFoundException ne) {
            return new ResponseEntity<>(ne.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
