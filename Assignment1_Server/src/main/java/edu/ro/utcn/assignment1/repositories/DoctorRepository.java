package edu.ro.utcn.assignment1.repositories;

import edu.ro.utcn.assignment1.entities.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DoctorRepository  extends JpaRepository<Doctor, String> {

}
