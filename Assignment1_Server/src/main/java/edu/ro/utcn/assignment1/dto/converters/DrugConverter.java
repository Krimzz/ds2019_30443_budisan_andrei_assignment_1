package edu.ro.utcn.assignment1.dto.converters;

import edu.ro.utcn.assignment1.dto.DrugDTO;
import edu.ro.utcn.assignment1.entities.Drug;

import java.util.ArrayList;
import java.util.List;

public class DrugConverter {

    private DrugConverter() {

    }

    public static DrugDTO toDto(Drug model) {
        DrugDTO dto = null;
        if (model != null) {
            dto = new DrugDTO(model.getId(),
                    model.getName(),
                    model.getIntakeStartTime(),
                    model.getIntakeEndTime(),
                    model.getDosage(),
                    model.getSideEffects());
        }
        return dto;
    }

    public static List<DrugDTO> toDto(List<Drug> models) {
        List<DrugDTO> drugDTOS = new ArrayList<>();
        if ((models != null) && (!models.isEmpty())) {
            for (Drug drug : models) {
                drugDTOS.add(toDto(drug));
            }
        }
        return drugDTOS;
    }
}
