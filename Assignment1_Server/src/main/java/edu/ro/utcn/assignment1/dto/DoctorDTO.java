package edu.ro.utcn.assignment1.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import edu.ro.utcn.assignment1.entities.Doctor;
import edu.ro.utcn.assignment1.entities.MedicationPlan;

import java.sql.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DoctorDTO extends UserDTO {

    private String domain;
    private List<MedicationPlan> medicationPlans;

    @JsonCreator
    public DoctorDTO(@JsonProperty("username") String username,
                     @JsonProperty("password") String password,
                     @JsonProperty("name") String name,
                     @JsonProperty("address") String address,
                     @JsonProperty("gender") String gender,
                     @JsonProperty("birthDate") String birthDate,
                     @JsonProperty("domain") String domain,
                     @JsonProperty("medicationPlans") List<MedicationPlan> medicationPlans) {
        super(username, password, name, Doctor.class.getSimpleName(), address, gender, birthDate);
        this.domain = domain;
        this.medicationPlans = medicationPlans;
    }

    @JsonProperty("domain")
    public String getDomain() {
        return domain;
    }

    @JsonProperty("medicationPlans")
    public List<MedicationPlan> getMedicationPlans() {
        return medicationPlans;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DoctorDTO{");
        sb.append("username='").append(getUsername()).append('\'');
        sb.append(", password='").append(getPassword()).append('\'');
        sb.append(", name='").append(getName()).append('\'');
        sb.append(", role='").append(getRole()).append('\'');
        sb.append(", address='").append(getAddress()).append('\'');
        sb.append(", gender='").append(getGender()).append('\'');
        sb.append(", birthDate='").append(getBirthDate()).append('\'');
        sb.append(", domain='").append(getDomain()).append('\'');
        sb.append(", medicationPlans=").append(getMedicationPlans());
        sb.append('}');
        return sb.toString();
    }
}
