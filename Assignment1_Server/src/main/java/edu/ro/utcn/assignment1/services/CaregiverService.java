package edu.ro.utcn.assignment1.services;

import edu.ro.utcn.assignment1.dto.CaregiverDTO;
import edu.ro.utcn.assignment1.dto.converters.CaregiverConverter;
import edu.ro.utcn.assignment1.entities.Caregiver;
import edu.ro.utcn.assignment1.entities.MedicationPlan;
import edu.ro.utcn.assignment1.entities.Patient;
import edu.ro.utcn.assignment1.repositories.CaregiverRepository;
import edu.ro.utcn.assignment1.repositories.PatientRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class CaregiverService {

    @Autowired
    private CaregiverRepository caregiverRepository;

    @Autowired
    private PatientRepository patientRepository;

    public List<CaregiverDTO> getAllCaregivers(){
        return CaregiverConverter.toDto(caregiverRepository.findAll());
    }

    public CaregiverDTO getCaregiverByUsername(String username) throws Exception {
        Caregiver caregiver = caregiverRepository.findById(username).orElse(null);
        if (caregiver == null) throw new NotFoundException("No caregiver found with that username");
        return CaregiverConverter.toDto(caregiver);
    }

    public CaregiverDTO createCaregiver(CaregiverDTO caregiverDTO) throws Exception {
        Caregiver caregiver = new Caregiver(caregiverDTO.getUsername(),
                getEncodedPassword(caregiverDTO.getPassword()),
                caregiverDTO.getName(),
                caregiverDTO.getAddress(),
                caregiverDTO.getGender(),
                caregiverDTO.getBirthDate(),
                caregiverDTO.getPatients());
        Caregiver newCaregiver = caregiverRepository.save(caregiver);
        return CaregiverConverter.toDto(newCaregiver);
    }

    public CaregiverDTO updateCaregiver(CaregiverDTO caregiverDTO) throws Exception {
        Caregiver caregiver = caregiverRepository.findById(caregiverDTO.getUsername()).orElse(null);
        if (caregiver == null) {
            throw new NotFoundException("No caregiver found with that username");
        }

        if(!caregiverDTO.getUsername().equals(caregiver.getUsername())) {
            caregiver.setUsername(caregiverDTO.getUsername());
        }

        if(!caregiverDTO.getPassword().equals(caregiver.getPassword())) {
            caregiver.setPassword(getEncodedPassword(caregiverDTO.getPassword()));
        }

        if(!caregiverDTO.getName().equals(caregiver.getName())) {
            caregiver.setName(caregiverDTO.getName());
        }

        if(!caregiverDTO.getAddress().equals(caregiver.getAddress())) {
            caregiver.setAddress(caregiverDTO.getAddress());
        }

        if(!caregiverDTO.getGender().equals(caregiver.getGender())) {
            caregiver.setGender(caregiverDTO.getGender());
        }

        if(!caregiverDTO.getBirthDate().equals(caregiver.getBirthDate())) {
            caregiver.setBirthDate(caregiverDTO.getBirthDate());
        }

        if(!caregiverDTO.getPatients().equals(caregiver.getPatients())) {
            caregiver.setPatients(caregiverDTO.getPatients());
        }

        return CaregiverConverter.toDto(caregiverRepository.save(caregiver));
    }

    public String getEncodedPassword(String password) {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(password);
    }

    public void deleteCaregiver(String username) throws Exception {
        Caregiver caregiver = caregiverRepository.findById(username).orElse(null);
        if (caregiver == null) {
            throw new NotFoundException("No caregiver with that username");
        }

        caregiverRepository.delete(caregiver);
    }

    public void addPatientToCaregiver(String patientUsername, String caregiverUsername) throws Exception {
        Caregiver caregiver = caregiverRepository.findById(caregiverUsername).orElse(null);
        if (caregiver == null) {
            throw new NotFoundException("No caregiver found with that username");
        }

        Patient patient = patientRepository.findById(patientUsername).orElse(null);
        if (patient == null) {
            throw new NotFoundException("No patient found with that username");
        }

        caregiver.getPatients().add(patient);

        patientRepository.save(patient);
        caregiverRepository.save(caregiver);
    }

    public void removePatientFromCaregiver(String patientUsername, String caregiverUsername) throws Exception {
        Caregiver caregiver = caregiverRepository.findById(caregiverUsername).orElse(null);
        if (caregiver == null) {
            throw new NotFoundException("No caregiver found with that username");
        }

        Patient patient = patientRepository.findById(patientUsername).orElse(null);
        if (patient == null) {
            throw new NotFoundException("No patient found with that username");
        }

        caregiver.getPatients().remove(patient);

        patientRepository.save(patient);
        caregiverRepository.save(caregiver);
    }
}
