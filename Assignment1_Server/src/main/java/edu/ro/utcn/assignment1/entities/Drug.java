package edu.ro.utcn.assignment1.entities;

import javax.persistence.*;

import java.util.List;

@Entity
@Table(name = "drug")
public class Drug {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "intake_start_time")
    private String intakeStartTime;

    @Column(name = "intake_end_time")
    private String intakeEndTime;

    @Column(name = "dosage")
    private Integer dosage;

    @OneToMany(cascade = CascadeType.ALL)
    private List<SideEffect> sideEffects;

    public Drug() {

    }

    public Drug(Integer id, String name, String intakeStartTime, String intakeEndTime, Integer dosage, List<SideEffect> sideEffects) {
        this.id = id;
        this.name = name;
        this.intakeStartTime = intakeStartTime;
        this.intakeEndTime = intakeEndTime;
        this.dosage = dosage;
        this.sideEffects = sideEffects;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIntakeStartTime() {
        return intakeStartTime;
    }

    public void setIntakeStartTime(String intakeStartTime) {
        this.intakeStartTime = intakeStartTime;
    }

    public String getIntakeEndTime() {
        return intakeEndTime;
    }

    public void setIntakeEndTime(String intakeEndTime) {
        this.intakeEndTime = intakeEndTime;
    }

    public Integer getDosage() {
        return dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }

    public List<SideEffect> getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(List<SideEffect> sideEffects) {
        this.sideEffects = sideEffects;
    }
}
