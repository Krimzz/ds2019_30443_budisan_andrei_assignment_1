package edu.ro.utcn.assignment1.services;

import edu.ro.utcn.assignment1.dto.PatientDTO;
import edu.ro.utcn.assignment1.dto.converters.PatientConverter;
import edu.ro.utcn.assignment1.entities.Caregiver;
import edu.ro.utcn.assignment1.entities.Doctor;
import edu.ro.utcn.assignment1.entities.MedicationPlan;
import edu.ro.utcn.assignment1.entities.Patient;
import edu.ro.utcn.assignment1.repositories.CaregiverRepository;
import edu.ro.utcn.assignment1.repositories.DoctorRepository;
import edu.ro.utcn.assignment1.repositories.PatientRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientService {

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private CaregiverRepository caregiverRepository;

    @Autowired
    private DoctorRepository doctorRepository;

    public List<PatientDTO> getAllPatients(){
        return PatientConverter.toDto(patientRepository.findAll());
    }

    public PatientDTO getPatientByUsername(String username) throws Exception {
        Patient patient = patientRepository.findById(username).orElse(null);
        if (patient == null) throw new NotFoundException("No patient found with that username");
        return PatientConverter.toDto(patient);
    }

    public PatientDTO createPatient(PatientDTO patientDTO) throws Exception {
        Patient patient = new Patient(patientDTO.getUsername(),
                getEncodedPassword(patientDTO.getPassword()),
                patientDTO.getName(),
                patientDTO.getAddress(),
                patientDTO.getGender(),
                patientDTO.getBirthDate(),
                patientDTO.getMedicationPlans());
        Patient newPatient = patientRepository.save(patient);
        return PatientConverter.toDto(newPatient);
    }

    public PatientDTO updatePatient(PatientDTO patientDTO) throws Exception {
        Patient patient = patientRepository.findById(patientDTO.getUsername()).orElse(null);
        if (patient == null) {
            throw new NotFoundException("No patient found with that username");
        }

        if(!patientDTO.getUsername().equals(patient.getUsername())) {
            patient.setUsername(patientDTO.getUsername());
        }

        if(!patientDTO.getPassword().equals(patient.getPassword())) {
            patient.setPassword(getEncodedPassword(patientDTO.getPassword()));
        }

        if(!patientDTO.getName().equals(patient.getName())) {
            patient.setName(patientDTO.getName());
        }

        if(!patientDTO.getAddress().equals(patient.getAddress())) {
            patient.setAddress(patientDTO.getAddress());
        }

        if(!patientDTO.getGender().equals(patient.getGender())) {
            patient.setGender(patientDTO.getGender());
        }

        if(!patientDTO.getBirthDate().equals(patient.getBirthDate())) {
            patient.setBirthDate(patientDTO.getBirthDate());
        }

        if(!patientDTO.getMedicationPlans().equals(patient.getMedicationPlans())) {
            patient.setMedicationPlans(patientDTO.getMedicationPlans());
        }

        return PatientConverter.toDto(patientRepository.save(patient));
    }

    public String getEncodedPassword(String password) {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(password);
    }

    public void deletePatient(String username) throws Exception {
        Patient patient = patientRepository.findById(username).orElse(null);
        if (patient == null) {
            throw new NotFoundException("No patient with that username");
        }

        List<Caregiver> caregivers = caregiverRepository.findAll();

        for (Caregiver caregiver: caregivers) {
            caregiver.getPatients().remove(patient);
            caregiverRepository.save(caregiver);
        }

        List<Doctor> doctors = doctorRepository.findAll();

        for (Doctor doctor: doctors) {
            for (MedicationPlan medicationPlan: patient.getMedicationPlans()) {
                doctor.getMedicationPlans().remove(medicationPlan);
            }
            doctorRepository.save(doctor);
        }

        patientRepository.delete(patient);
    }
}
