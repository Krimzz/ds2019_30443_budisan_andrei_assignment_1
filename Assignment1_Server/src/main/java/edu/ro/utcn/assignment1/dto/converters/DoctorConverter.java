package edu.ro.utcn.assignment1.dto.converters;

import edu.ro.utcn.assignment1.dto.DoctorDTO;
import edu.ro.utcn.assignment1.entities.Doctor;

import java.util.ArrayList;
import java.util.List;

public class DoctorConverter {

    private DoctorConverter() {

    }

    public static DoctorDTO toDto(Doctor model) {
        DoctorDTO dto = null;
        if (model != null) {
            dto = new DoctorDTO(model.getUsername(),
                    model.getPassword(),
                    model.getName(),
                    model.getAddress(),
                    model.getGender(),
                    model.getBirthDate(),
                    model.getDomain(),
                    model.getMedicationPlans());
        }
        return dto;
    }

    public static List<DoctorDTO> toDto(List<Doctor> models) {
        List<DoctorDTO> doctorDTOS = new ArrayList<>();
        if ((models != null) && (!models.isEmpty())) {
            for (Doctor doctor : models) {
                doctorDTOS.add(toDto(doctor));
            }
        }
        return doctorDTOS;
    }
}
