package edu.ro.utcn.assignment1.entities;

import javax.persistence.*;

import java.util.List;

@Entity
@Table(name = "medication_plan")
public class MedicationPlan {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "start_date")
    private String startDate;

    @Column(name = "end_date")
    private String endDate;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Drug> drugs;

    public MedicationPlan() {

    }

    public MedicationPlan(Integer id, String startDate, String endDate, List<Drug> drugs) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.drugs = drugs;
    }

    public Integer getId() {
        return id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public List<Drug> getDrugs() {
        return drugs;
    }

    public void setDrugs(List<Drug> drugs) {
        this.drugs = drugs;
    }
}
