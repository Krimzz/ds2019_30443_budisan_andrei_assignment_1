package edu.ro.utcn.assignment1.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDTO {

    private String username;
    private String password;
    private String name;
    private String role;
    private String address;
    private String gender;
    private String birthDate;

    @JsonCreator
    public UserDTO(@JsonProperty("username") String username,
                   @JsonProperty("password") String password,
                   @JsonProperty("name") String name,
                   @JsonProperty("role") String role,
                   @JsonProperty("address") String address,
                   @JsonProperty("gender") String gender,
                   @JsonProperty("birthDate") String birthDate) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.role = role;
        this.address = address;
        this.gender = gender;
        this.birthDate = birthDate;
    }

    @JsonProperty("username")
    public String getUsername() {
        return username;
    }

    @JsonProperty("password")
    public String getPassword() {
        return password;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("role")
    public String getRole() {
        return role;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("gender")
    public String getGender() {
        return gender;
    }

    @JsonProperty("birthDate")
    public String getBirthDate() {
        return birthDate;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserDTO{");
        sb.append("username='").append(getUsername()).append('\'');
        sb.append(", password='").append(getPassword()).append('\'');
        sb.append(", name='").append(getName()).append('\'');
        sb.append(", role='").append(getRole()).append('\'');
        sb.append(", address='").append(getAddress()).append('\'');
        sb.append(", gender='").append(getGender()).append('\'');
        sb.append(", birthDate=").append(getBirthDate());
        sb.append('}');
        return sb.toString();
    }
}
