package edu.ro.utcn.assignment1.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import edu.ro.utcn.assignment1.entities.Caregiver;
import edu.ro.utcn.assignment1.entities.Patient;

import java.util.List;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CaregiverDTO extends UserDTO{

    private Set<Patient> patients;

    @JsonCreator
    public CaregiverDTO(@JsonProperty("username") String username,
                        @JsonProperty("password") String password,
                        @JsonProperty("name") String name,
                        @JsonProperty("address") String address,
                        @JsonProperty("gender") String gender,
                        @JsonProperty("birthDate") String birthDate,
                        @JsonProperty("patients") Set<Patient> patients) {
        super(username, password, name, Caregiver.class.getSimpleName(), address, gender, birthDate);
        this.patients = patients;
    }

    @JsonProperty("patients")
    public Set<Patient> getPatients() {
        return patients;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CaregiverDTO{");
        sb.append("username='").append(getUsername()).append('\'');
        sb.append(", password='").append(getPassword()).append('\'');
        sb.append(", name='").append(getName()).append('\'');
        sb.append(", role='").append(getRole()).append('\'');
        sb.append(", address='").append(getAddress()).append('\'');
        sb.append(", gender='").append(getGender()).append('\'');
        sb.append(", birthDate='").append(getBirthDate()).append('\'');
        sb.append(", patients=").append(getPatients());
        sb.append('}');
        return sb.toString();
    }
}
