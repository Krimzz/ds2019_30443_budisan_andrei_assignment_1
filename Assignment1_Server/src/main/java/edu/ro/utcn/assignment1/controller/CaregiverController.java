package edu.ro.utcn.assignment1.controller;

import edu.ro.utcn.assignment1.dto.CaregiverDTO;
import edu.ro.utcn.assignment1.services.CaregiverService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("medicalplatform")
@CrossOrigin
public class CaregiverController {

    @Autowired
    private CaregiverService caregiverService;

    @GetMapping(value = "/doctor/caregiver")
    public List<CaregiverDTO> getAllCaregivers() {
        return caregiverService.getAllCaregivers();
    }

    @GetMapping(value = {"/doctor/caregiver/{username}" , "/caregiver/{username}"})
    public ResponseEntity<?> findCaregiverByUsername(@PathVariable("username") String username) {
        try {
            CaregiverDTO caregiverDTO = caregiverService.getCaregiverByUsername(username);
            return new ResponseEntity<>(caregiverDTO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/doctor/caregiver")
    public ResponseEntity<?> insertCaregiver(@RequestBody CaregiverDTO caregiverDTO) {
        CaregiverDTO caregiverDTOtoBeInserted;
        try {
            caregiverDTOtoBeInserted = caregiverService.createCaregiver(caregiverDTO);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(caregiverDTOtoBeInserted, HttpStatus.CREATED);
    }

    @PutMapping(value = "/doctor/caregiver")
    public ResponseEntity<?> updateCaregiver(@RequestBody CaregiverDTO caregiverDTO) {
        try {
            CaregiverDTO changedCaregiver = caregiverService.updateCaregiver(caregiverDTO);
            return new ResponseEntity<>(changedCaregiver, HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value = "/doctor/caregiver/{username}")
    public ResponseEntity<?> deleteCaregiver(@PathVariable("username") String username) {
        try {
            caregiverService.deleteCaregiver(username);
            return new ResponseEntity<String>(HttpStatus.OK);
        } catch (NotFoundException ne) {
            return new ResponseEntity<>(ne.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/doctor/caregiver/add/{patientUsername}/{caregiverUsername}")
    public ResponseEntity<?> addPatientToCaregiver(@PathVariable("patientUsername") String patientUsername, @PathVariable("caregiverUsername") String caregiverUsername) {
        try {
            caregiverService.addPatientToCaregiver(patientUsername, caregiverUsername);
            return new ResponseEntity<String>(HttpStatus.OK);
        } catch (NotFoundException ne) {
            return new ResponseEntity<>(ne.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/doctor/caregiver/remove/{patientUsername}/{caregiverUsername}")
    public ResponseEntity<?> removePatientFromCaregiver(@PathVariable("patientUsername") String patientUsername, @PathVariable("caregiverUsername") String caregiverUsername) {
        try {
            caregiverService.removePatientFromCaregiver(patientUsername, caregiverUsername);
            return new ResponseEntity<String>(HttpStatus.OK);
        } catch (NotFoundException ne) {
            return new ResponseEntity<>(ne.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
