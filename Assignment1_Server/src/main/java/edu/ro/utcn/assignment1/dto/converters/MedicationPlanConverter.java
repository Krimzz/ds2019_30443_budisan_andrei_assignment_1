package edu.ro.utcn.assignment1.dto.converters;

import edu.ro.utcn.assignment1.dto.MedicationPlanDTO;
import edu.ro.utcn.assignment1.entities.MedicationPlan;

import java.util.ArrayList;
import java.util.List;

public class MedicationPlanConverter {
    
    private MedicationPlanConverter() {

    }

    public static MedicationPlanDTO toDto(MedicationPlan model) {
        MedicationPlanDTO dto = null;
        if (model != null) {
            dto = new MedicationPlanDTO(model.getId(),
                    model.getStartDate(),
                    model.getEndDate(),
                    model.getDrugs());
        }
        return dto;
    }

    public static List<MedicationPlanDTO> toDto(List<MedicationPlan> models) {
        List<MedicationPlanDTO> medicationPlanDTOS = new ArrayList<>();
        if ((models != null) && (!models.isEmpty())) {
            for (MedicationPlan medicationPlan : models) {
                medicationPlanDTOS.add(toDto(medicationPlan));
            }
        }
        return medicationPlanDTOS;
    }
}
