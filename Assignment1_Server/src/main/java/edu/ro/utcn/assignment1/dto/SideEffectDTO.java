package edu.ro.utcn.assignment1.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SideEffectDTO {

    private Integer id;
    private String description;

    @JsonCreator
    public SideEffectDTO(@JsonProperty("id") Integer id,
                         @JsonProperty("description") String description) {
        this.id = id;
        this.description = description;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SideEffectDTO{");
        sb.append("id='").append(getId()).append('\'');
        sb.append(", description=").append(getDescription());
        sb.append('}');
        return sb.toString();
    }
}
