package edu.ro.utcn.assignment1.entities;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@PrimaryKeyJoinColumn(name = "username")
@Table(name = "patient")
public class Patient extends User {

    @OneToMany(cascade = CascadeType.ALL)
    private List<MedicationPlan> medicationPlans;

    public Patient() {

    }

    public Patient(String username,
                   String password,
                   String name,
                   String address,
                   String gender,
                   String birthDate,
                   List<MedicationPlan> medicationPlans) {
        super(username, password, name, Patient.class.getSimpleName(), address, gender, birthDate);
        this.medicationPlans  = medicationPlans;
    }

    public List<MedicationPlan> getMedicationPlans() {
        return medicationPlans;
    }

    public void setMedicationPlans(List<MedicationPlan> medicationPlans) {
        this.medicationPlans = medicationPlans;
    }
}
