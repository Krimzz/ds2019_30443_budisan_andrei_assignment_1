package edu.ro.utcn.assignment1.repositories;

import edu.ro.utcn.assignment1.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatientRepository extends JpaRepository<Patient, String> {

}
