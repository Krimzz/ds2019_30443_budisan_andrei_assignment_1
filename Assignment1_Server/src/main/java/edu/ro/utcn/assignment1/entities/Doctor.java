package edu.ro.utcn.assignment1.entities;

import javax.persistence.*;
import java.util.List;

@Entity
@PrimaryKeyJoinColumn(name = "username")
@Table(name = "doctor")
public class Doctor extends User {

    @Column(name = "domain")
    private String domain;

    @OneToMany(cascade = CascadeType.ALL)
    private List<MedicationPlan> medicationPlans;

    public Doctor() {

    }

    public Doctor(String username,
                  String password,
                  String name,
                  String address,
                  String gender,
                  String birthDate,
                  String domain,
                  List<MedicationPlan> medicationPlans) {
        super(username, password, name, Doctor.class.getSimpleName(), address, gender, birthDate);
        this.domain = domain;
        this.medicationPlans = medicationPlans;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public List<MedicationPlan> getMedicationPlans() {
        return medicationPlans;
    }

    public void setMedicationPlans(List<MedicationPlan> medicationPlans) {
        this.medicationPlans = medicationPlans;
    }
}
