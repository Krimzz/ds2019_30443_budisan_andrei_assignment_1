package edu.ro.utcn.assignment1.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import edu.ro.utcn.assignment1.entities.Caregiver;
import edu.ro.utcn.assignment1.entities.MedicationPlan;
import edu.ro.utcn.assignment1.entities.Patient;

import java.sql.Date;
import java.util.List;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PatientDTO extends UserDTO {

    private List<MedicationPlan> medicationPlans;

    public PatientDTO(@JsonProperty("username") String username,
                      @JsonProperty("password") String password,
                      @JsonProperty("name") String name,
                      @JsonProperty("address") String address,
                      @JsonProperty("gender") String gender,
                      @JsonProperty("birthDate") String birthDate,
                      @JsonProperty("medicationPlans") List<MedicationPlan> medicationPlans) {
        super(username, password, name, Patient.class.getSimpleName(), address, gender, birthDate);
        this.medicationPlans = medicationPlans;
    }

    @JsonProperty("medicationPlans")
    public List<MedicationPlan> getMedicationPlans() {
        return medicationPlans;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PatientDTO{");
        sb.append("username='").append(getUsername()).append('\'');
        sb.append(", password='").append(getPassword()).append('\'');
        sb.append(", name='").append(getName()).append('\'');
        sb.append(", role='").append(getRole()).append('\'');
        sb.append(", address='").append(getAddress()).append('\'');
        sb.append(", gender='").append(getGender()).append('\'');
        sb.append(", birthDate='").append(getBirthDate()).append('\'');
        sb.append(", medicationPlans='").append(getMedicationPlans());
        sb.append('}');
        return sb.toString();
    }
}
