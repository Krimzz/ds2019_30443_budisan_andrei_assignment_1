package edu.ro.utcn.assignment1.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import edu.ro.utcn.assignment1.entities.SideEffect;

import java.sql.Time;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DrugDTO {

    private Integer id;
    private String name;
    private String intakeStartTime;
    private String intakeEndTime;
    private Integer dosage;
    private List<SideEffect> sideEffects;

    @JsonCreator
    public DrugDTO(@JsonProperty("id") Integer id,
                   @JsonProperty("name") String name,
                   @JsonProperty("intakeStartTime") String intakeStartTime,
                   @JsonProperty("intakeEndTime") String intakeEndTime,
                   @JsonProperty("dosage") Integer dosage,
                   @JsonProperty("sideEffects") List<SideEffect> sideEffects) {
        this.id = id;
        this.name = name;
        this.intakeStartTime = intakeStartTime;
        this.intakeEndTime = intakeEndTime;
        this.dosage = dosage;
        this.sideEffects = sideEffects;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("intakeStartTime")
    public String getIntakeStartTime() {
        return intakeStartTime;
    }

    @JsonProperty("intakeEndTime")
    public String getIntakeEndTime() {
        return intakeEndTime;
    }

    @JsonProperty("dosage")
    public Integer getDosage() {
        return dosage;
    }

    @JsonProperty("sideEffects")
    public List<SideEffect> getSideEffects() {
        return sideEffects;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DrugDTO{");
        sb.append("id='").append(getId()).append('\'');
        sb.append(", name='").append(getName()).append('\'');
        sb.append(", intakeStartTime='").append(getIntakeStartTime()).append('\'');
        sb.append(", intakeEndTime='").append(getIntakeEndTime()).append('\'');
        sb.append(", dosage='").append(getDosage()).append('\'');
        sb.append(", sideEffects=").append(getSideEffects());
        sb.append('}');
        return sb.toString();
    }
}
