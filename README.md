# DS2019_30443_Budisan_Andrei_Assignment_1

##Development Requirements
Install:
- Java 8
- mysql 

## Getting started
To start the service you need to go to the application.proprieties and change the datasource for URL,USERNAME,PASSWORD

## Starting the service
Go to bankingservice folder and execute the following command: 
gradlew.bat bootRun

## Accessing the API service:
- http://localhost:8080/medicalplan

doctor apis: 
- http://localhost:8080/doctor/**
- http://localhost:8080/doctor/caregiver/**
- http://localhost:8080/doctor/patient/**
- http://localhost:8080/doctor/medicationplan
- http://localhost:8080/login

caregiver apis: 
- http://localhost:8080/caregiver/**

patient apis: 
- http://localhost:8080/patient/**

## Calling the service apis:
- each api call is protected with credentials that are configured in https://github.com/UTCN-SD-PS/banking-service/blob/master/src/main/resources/application.properties

## Operations that can be done:
- CRUD operations on all the tables
- Add medication plan to a patient and remove the medication plan from the patient
- Add drug to medication plan and remove it
- Add patient to caregiver and remove it

## Utilities
- All passwords are encrypted and the information on each user is stored in the employee_table
