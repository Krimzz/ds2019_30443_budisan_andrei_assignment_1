import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DoctorInterface} from '../../../shared/models/interfaces/doctor';
import {MedicationPlanInterface} from '../../../shared/models/interfaces/medicationPlan';
import {ApiService} from '../../../shared/services/api.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-add-doctors',
  templateUrl: './add-doctors.component.html',
  styleUrls: ['./add-doctors.component.css']
})
export class AddDoctorsComponent implements OnInit {
  username: string;
  password: string;
  name: string;
  address: string;
  gender: string;
  birthDate: string;
  domain: string;

  private doctorData: DoctorInterface;

  constructor(private apiService: ApiService) { }

  ngOnInit() {

  }

  addDoctor() {
    this.doctorData = {
      username: this.username,
      password: this.password,
      name: this.name,
      address: this.address,
      gender: this.gender,
      birthDate: this.birthDate,
      domain: this.domain,
      medicationPlans: new Array<MedicationPlanInterface>()
    };

    this.apiService.insertDoctor(this.doctorData).subscribe(
      (data: DoctorInterface) => {
        console.log('Doctor added: ' + data);
      },
      (error: HttpErrorResponse) => {
        console.error('Doctor not added with error: ' + error.message);
      }
    );
  }
}
