import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyDoctorsComponent } from './modify-doctors.component';

describe('RemoveDoctorsComponent', () => {
  let component: ModifyDoctorsComponent;
  let fixture: ComponentFixture<ModifyDoctorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyDoctorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyDoctorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
