import {Component, OnInit, NgModule, Input, OnDestroy} from '@angular/core';
import {Observable} from 'rxjs';
import {DoctorInterface} from '../../../shared/models/interfaces/doctor';
import {HttpErrorResponse} from '@angular/common/http';
import {ApiService} from '../../../shared/services/api.service';

@Component({
  selector: 'app-remove-doctors',
  templateUrl: './modify-doctors.component.html',
  styleUrls: ['./modify-doctors.component.css']
})
export class ModifyDoctorsComponent implements OnInit {

  doctors: Array<DoctorInterface>;

  constructor(private apiService: ApiService) {

  }

  ngOnInit() {
    this.getDoctors();
  }

  getDoctors() {
    this.apiService.getDoctors().subscribe(
      (data: Array<DoctorInterface>) => (this.doctors = data),
      (error: HttpErrorResponse) => {
        console.error('Could not get doctors with error: ' + error.message);
      }
    );
  }

  onRemove(doctor: DoctorInterface) {
    this.apiService.deleteDoctor(doctor.username).subscribe(
      () => {
        const index = this.doctors.indexOf(doctor, 0);
        if (index > -1) {
          this.doctors.splice(index, 1);
        }
        console.log('Doctor was removed!');
      },
      (error: HttpErrorResponse) => {
        console.error('Could not remove doctor with error: ' + error.message);
      }
    );
  }

  onSave(doctor: DoctorInterface) {
    this.apiService.saveDoctorDetails(doctor).subscribe(
      (data: DoctorInterface) => {
        const index = this.doctors.indexOf(doctor);
        if (index !== -1) {
          this.doctors[index] = data;
        }
        console.log('Doctor was updated!');
      },
      (error: HttpErrorResponse) => {
        console.error('Could not update doctor with error: ' + error.message);
      },
    );
  }

  changeValue(id: number, property: string, event: any) {
    this.doctors[id][property] = event.target.textContent;
  }
}
