import {Component, Input, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-crud-doctors',
  templateUrl: './crud-doctors.component.html',
  styleUrls: ['./crud-doctors.component.css']
})
export class CrudDoctorsComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }

  addDoctor() {
    this.router.navigate(['addDoctor'], {relativeTo: this.route});
  }

  removeDoctors() {
    this.router.navigate(['modifyDoctor'], {relativeTo: this.route});
  }
}
