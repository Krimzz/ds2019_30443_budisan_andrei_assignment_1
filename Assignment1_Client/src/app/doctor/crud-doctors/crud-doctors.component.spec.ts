import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudDoctorsComponent } from './crud-doctors.component';

describe('CrudDoctorsComponent', () => {
  let component: CrudDoctorsComponent;
  let fixture: ComponentFixture<CrudDoctorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudDoctorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudDoctorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
