import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyMedicationPlanComponent } from './modify-medication-plan.component';

describe('ModifyMedicalPlanComponent', () => {
  let component: ModifyMedicationPlanComponent;
  let fixture: ComponentFixture<ModifyMedicationPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyMedicationPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyMedicationPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
