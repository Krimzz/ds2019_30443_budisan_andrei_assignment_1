import { Component, OnInit } from '@angular/core';
import { DrugInterface } from 'src/app/shared/models/interfaces/drug';
import { SideEffectInterface } from 'src/app/shared/models/interfaces/sideEffect';
import {StorageService} from '../../../../shared/services/storage.service';
import {ApiService} from '../../../../shared/services/api.service';
import {MedicationPlanInterface} from '../../../../shared/models/interfaces/medicationPlan';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-modify-drugs',
  templateUrl: './modify-drugs.component.html',
  styleUrls: ['./modify-drugs.component.css']
})
export class ModifyDrugsComponent implements OnInit {

  drugs: Array<DrugInterface>;

  constructor(private storageService: StorageService, private apiService: ApiService) {
  }

  ngOnInit() {
    const medicationPlanId = +this.storageService.get(this.storageService.medicationPlanId);
    this.apiService.getMedicationPlanDetails(medicationPlanId).subscribe(
      (data: MedicationPlanInterface) => {
        this.drugs = data.drugs;
        console.log('Got drugs from medication plan');
      },
      (error: HttpErrorResponse) => {
        console.error('Could not get medication plan: ' + error.message);
      }
    )
  }


  onSave(){
    const medicationPlanId = +this.storageService.get(this.storageService.medicationPlanId);
    this.apiService.getMedicationPlanDetails(medicationPlanId).subscribe(
      (data: MedicationPlanInterface) => {
        data.drugs = this.drugs;

        this.apiService.saveMedicationPlanDetails(data).subscribe(
          (data: MedicationPlanInterface) => {
            this.drugs = data.drugs;
            console.log('Updated drugs in medication plan');
          },
          (error: HttpErrorResponse) => {
            console.error('Could not update drugs in medication plan: ' + error.message);
          }
        )
      },
      (error: HttpErrorResponse) => {
        console.error('Could not get medication plan: ' + error.message);
      }
    )
  }

  onRemove(drugToRemoveId){
    const medicationPlanId = +this.storageService.get(this.storageService.medicationPlanId);
    this.apiService.getMedicationPlanDetails(medicationPlanId).subscribe(
      (data: MedicationPlanInterface) => {

        data.drugs.splice(drugToRemoveId, 1);

        this.apiService.saveMedicationPlanDetails(data).subscribe(
          (data: MedicationPlanInterface) => {
            this.drugs = data.drugs;
            console.log('Removed drug from medication plan');
          },
          (error: HttpErrorResponse) => {
            console.error('Could not remove drug from medication plan: ' + error.message);
          }
        )
      },
      (error: HttpErrorResponse) => {
        console.error('Could not get medication plan: ' + error.message);
      }
    )
  }

  changeValue(id: number, property: string, event: any) {
    this.drugs[id][property] = event.target.textContent;
  }
}
