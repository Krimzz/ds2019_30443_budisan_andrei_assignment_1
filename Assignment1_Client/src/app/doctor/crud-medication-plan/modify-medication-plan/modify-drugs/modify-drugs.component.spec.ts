import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyDrugsComponent } from './modify-drugs.component';

describe('ViewMedicineComponent', () => {
  let component: ModifyDrugsComponent;
  let fixture: ComponentFixture<ModifyDrugsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyDrugsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyDrugsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
