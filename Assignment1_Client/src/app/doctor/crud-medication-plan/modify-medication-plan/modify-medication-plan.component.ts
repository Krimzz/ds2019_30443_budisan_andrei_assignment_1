import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { ApiService } from 'src/app/shared/services/api.service';
import { MedicationPlanInterface } from 'src/app/shared/models/interfaces/medicationPlan';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import {StorageService} from '../../../shared/services/storage.service';

@Component({
  selector: 'app-modify-medication-plan',
  templateUrl: './modify-medication-plan.component.html',
  styleUrls: ['./modify-medication-plan.component.css']
})
export class ModifyMedicationPlanComponent implements OnInit {

  medicationPlans: Array<MedicationPlanInterface>;

  constructor(private apiService: ApiService,private route: ActivatedRoute, private router: Router, private storageService: StorageService) {

  }

  ngOnInit() {
    this.getMedicationPlans();
  }

  showDrugs(medicationPlanId: number){
    this.storageService.set(this.storageService.medicationPlanId, String(medicationPlanId));
    this.router.navigate(['viewDrugs'], {relativeTo: this.route});
  }

  addDrugs(medicationPlanId: number){
    this.storageService.set(this.storageService.medicationPlanId, String(medicationPlanId));
    this.router.navigate(['showDrugs'], {relativeTo: this.route});
  }

  getMedicationPlans() {
    this.apiService.getMedicationPlans().subscribe(
      (data: Array<MedicationPlanInterface>) => (this.medicationPlans = data),
      (error: HttpErrorResponse) => {
        console.error('Could not get medical with error: ' + error.message);
      }
    );
  }

  onRemove(medicationPlan: MedicationPlanInterface) {
    this.apiService.deleteMedicationPlan(medicationPlan.id).subscribe(
      () => {
        const index = this.medicationPlans.indexOf(medicationPlan, 0);
        if (index > -1) {
          this.medicationPlans.splice(index, 1);
        }
        console.log('Medication plan was removed!');
      },
      (error: HttpErrorResponse) => {
        console.error('Could not remove medication plan with error: ' + error.message);
      }
    );
  }

  onSave(medicationPlan: MedicationPlanInterface) {
    this.apiService.saveMedicationPlanDetails(medicationPlan).subscribe(
      (data: MedicationPlanInterface) => {
        const index = this.medicationPlans.indexOf(medicationPlan);
        if (index !== -1) {
          this.medicationPlans[index] = data;
        }
        console.log('Medication plan was updated!');
      },
      (error: HttpErrorResponse) => {
        console.error('Could not update medication plan with error: ' + error.message);
      },
    );
  }

  changeValue(id: number, property: string, event: any) {
    this.medicationPlans[id][property] = event.target.textContent;
  }

}
