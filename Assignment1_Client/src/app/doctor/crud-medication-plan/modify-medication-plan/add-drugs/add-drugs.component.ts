import {Component, Input, OnInit} from '@angular/core';
import {StorageService} from '../../../../shared/services/storage.service';
import {ApiService} from '../../../../shared/services/api.service';
import {MedicationPlanInterface} from '../../../../shared/models/interfaces/medicationPlan';
import {HttpErrorResponse} from '@angular/common/http';
import {DrugInterface} from '../../../../shared/models/interfaces/drug';
import {SideEffectInterface} from '../../../../shared/models/interfaces/sideEffect';

@Component({
  selector: 'app-add-drugs',
  templateUrl: './add-drugs.component.html',
  styleUrls: ['./add-drugs.component.css']
})
export class AddDrugsComponent implements OnInit {

  intakeStartTime:string;
  intakeEndTime:string;
  name:string;
  dosage:number;

  constructor(private storageService: StorageService, private apiService: ApiService) { }

  ngOnInit() {
  }

  addDrug(){
    const medicationPlanId = +this.storageService.get(this.storageService.medicationPlanId);
    this.apiService.getMedicationPlanDetails(medicationPlanId).subscribe(
      (data: MedicationPlanInterface) => {
        const drug: DrugInterface = {
          id: null,
          name: this.name,
          dosage: this.dosage,
          intakeStartTime: this.intakeStartTime,
          intakeEndTime: this.intakeEndTime,
          sideEffects: new Array<SideEffectInterface>()
        };

        const medicationPlan = data;

        medicationPlan.drugs.push(drug);

        this.apiService.saveMedicationPlanDetails(medicationPlan).subscribe(
          (data: MedicationPlanInterface) => {
            console.log('Added new drug to medication plan: ' + data);
          },
          (error: HttpErrorResponse) => {
            console.error('Could not add new drug to medication plan with error: ' + error.message);
          }
        )
      },
      (error: HttpErrorResponse) => {
        console.error('Could not get medication plan with error: ' + error.message);
      }
    )
  }

}
