import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-crud-medical-plan',
  templateUrl: './crud-medication-plan.component.html',
  styleUrls: ['./crud-medication-plan.component.css']
})
export class CrudMedicationPlanComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router) { }

  addMedicalPlan() {
    this.router.navigate(['addMedicinePlan'], {relativeTo: this.route});
  }

  modifyMedicalPlan() {
    this.router.navigate(['modifyMedicinePlan'], {relativeTo: this.route});
  }

  ngOnInit() {
  }

}
