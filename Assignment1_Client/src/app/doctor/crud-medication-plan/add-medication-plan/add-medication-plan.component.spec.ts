import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMedicationPlanComponent } from './add-medication-plan.component';

describe('AddMedicinePlanComponent', () => {
  let component: AddMedicationPlanComponent;
  let fixture: ComponentFixture<AddMedicationPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMedicationPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMedicationPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
