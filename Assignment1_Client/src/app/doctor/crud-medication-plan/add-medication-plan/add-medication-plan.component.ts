import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/shared/services/api.service';
import { MedicationPlanInterface } from 'src/app/shared/models/interfaces/medicationPlan';
import { DrugInterface } from 'src/app/shared/models/interfaces/drug';
import { HttpErrorResponse } from '@angular/common/http';
import {StorageService} from '../../../shared/services/storage.service';

@Component({
  selector: 'app-add-medication-plan',
  templateUrl: './add-medication-plan.component.html',
  styleUrls: ['./add-medication-plan.component.css']
})
export class AddMedicationPlanComponent implements OnInit {

  patientUsername: string;
  startDate: string;
  endDate: string;

  medicalData: MedicationPlanInterface;

  constructor(private apiService: ApiService, private storageService: StorageService) { }

  ngOnInit() {

  }

  addMedicationPlan() {
    this.medicalData = {
      id: null,
      startDate: this.startDate,
      endDate: this.endDate,
      drugs: new Array<DrugInterface>()
    };

    const doctorId = this.storageService.get(this.storageService.loggedInUsername);

    this.apiService.addMedicationPlanToPatient(doctorId, this.patientUsername, this.medicalData).subscribe(
      (data: MedicationPlanInterface) => {
        console.log('Medical added: ' + data);
      },
      (error: HttpErrorResponse) => {
        console.error('Medical not added with error: ' + error.message);
      }
    )
  }
}
