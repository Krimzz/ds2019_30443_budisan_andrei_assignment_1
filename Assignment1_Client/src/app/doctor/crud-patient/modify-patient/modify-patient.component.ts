import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/shared/services/api.service';
import { PatientInterface } from 'src/app/shared/models/interfaces/patient';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-modify-patient',
  templateUrl: './modify-patient.component.html',
  styleUrls: ['./modify-patient.component.css']
})
export class ModifyPatientComponent implements OnInit {

  patients: Array<PatientInterface>;

  constructor(private apiService: ApiService) {

  }

  ngOnInit() {
    this.getDoctors();
  }

  getDoctors() {
    this.apiService.getPatients().subscribe(
      (data: Array<PatientInterface>) => (this.patients = data),
      (error: HttpErrorResponse) => {
        console.error('Could not get doctors with error: ' + error.message);
      }
    );
  }

  onRemove(patient: PatientInterface) {
    this.apiService.deletePatient(patient.username).subscribe(
      () => {
        const index = this.patients.indexOf(patient, 0);
        if (index > -1) {
          this.patients.splice(index, 1);
        }
        console.log('Patient was removed!');
      },
      (error: HttpErrorResponse) => {
        console.error('Could not remove patient with error: ' + error.message);
      }
    );
  }

  onSave(patient: PatientInterface) {
    this.apiService.savePatientDetails(patient).subscribe(
      (data: PatientInterface) => {
        const index = this.patients.indexOf(patient);
        if (index !== -1) {
          this.patients[index] = data;
        }
        console.log('Patient was updated!');
      },
      (error: HttpErrorResponse) => {
        console.error('Could not update patient with error: ' + error.message);
      },
    );
  }

  changeValue(id: number, property: string, event: any) {
    this.patients[id][property] = event.target.textContent;
  }

}
