import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-crud-patient',
  templateUrl: './crud-patient.component.html',
  styleUrls: ['./crud-patient.component.css']
})
export class CrudPatientComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router) { }

  addPatient() {
    this.router.navigate(['addPatient'], {relativeTo: this.route});
  }

  modifyPatient() {
    this.router.navigate(['modifyPatient'], {relativeTo: this.route});
  }

  ngOnInit() {
  }

}
