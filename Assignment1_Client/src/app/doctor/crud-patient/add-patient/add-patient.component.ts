import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/shared/services/api.service';
import { PatientInterface } from 'src/app/shared/models/interfaces/patient';
import { MedicationPlanInterface } from 'src/app/shared/models/interfaces/medicationPlan';
import { HttpErrorResponse } from '@angular/common/http';
import { CaregiverInterface } from 'src/app/shared/models/interfaces/caregiver';

@Component({
  selector: 'app-add-patient',
  templateUrl: './add-patient.component.html',
  styleUrls: ['./add-patient.component.css']
})
export class AddPatientComponent implements OnInit {

  constructor(private apiService: ApiService) { }

  username: string;
  password: string;
  name: string;
  address: string;
  gender: string;
  birthDate: string;

  private patientData: PatientInterface;


  ngOnInit() {

  }

  addDoctor() {
    this.patientData = {
      username: this.username,
      password: this.password,
      name: this.name,
      address: this.address,
      gender: this.gender,
      birthDate: this.birthDate,
      medicationPlans: new Array<MedicationPlanInterface>()
    };

    this.apiService.insertPatient(this.patientData).subscribe(
      (data: PatientInterface) => {
        console.log('Doctor added: ' + data);
      },
      (error: HttpErrorResponse) => {
        console.error('Doctor not added with error: ' + error.message);
      }
    );
  }


}
