import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/shared/services/api.service';
import { CaregiverInterface } from 'src/app/shared/models/interfaces/caregiver';
import { HttpErrorResponse } from '@angular/common/http';
import { PatientInterface } from 'src/app/shared/models/interfaces/patient';


@Component({
  selector: 'app-add-caregiver',
  templateUrl: './add-caregiver.component.html',
  styleUrls: ['./add-caregiver.component.css']
})
export class AddCaregiverComponent implements OnInit {
  username: string;
  password: string;
  name: string;
  address: string;
  gender: string;
  birthDate: string;

  private caregiverData: CaregiverInterface;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
  }

  addCaregiver() {
    this.caregiverData = {
      username: this.username,
      password: this.password,
      name: this.name,
      address: this.address,
      gender: this.gender,
      birthDate: this.birthDate,
      patients: new Array<PatientInterface>()
    };

    this.apiService.insertCaregiver(this.caregiverData).subscribe(
      (data: CaregiverInterface) => {
        console.log('Caregiver added: ' + data);
      },
      (error: HttpErrorResponse) => {
        console.error('Doctor not added with error: ' + error.message);
      }
    );
  }

}
