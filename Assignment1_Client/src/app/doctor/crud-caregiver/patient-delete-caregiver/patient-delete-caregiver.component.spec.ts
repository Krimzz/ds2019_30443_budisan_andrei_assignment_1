import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientDeleteCaregiverComponent } from './patient-delete-caregiver.component';

describe('PatientDeleteCaregiverComponent', () => {
  let component: PatientDeleteCaregiverComponent;
  let fixture: ComponentFixture<PatientDeleteCaregiverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientDeleteCaregiverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientDeleteCaregiverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
