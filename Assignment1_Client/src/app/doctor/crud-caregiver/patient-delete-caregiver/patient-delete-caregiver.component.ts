import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../shared/services/api.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-patient-delete-caregiver',
  templateUrl: './patient-delete-caregiver.component.html',
  styleUrls: ['./patient-delete-caregiver.component.css']
})
export class PatientDeleteCaregiverComponent implements OnInit {

  patient: string;
  caregiver:string;
  constructor(private apiService: ApiService) { }

  ngOnInit() {
  }

  deletePatientToCaregiver(){
    this.apiService.removePatientFromCaregiver(this.patient,this.caregiver).subscribe(
      () => {
        console.log('Patient removed from caregiver!');
      },
      (error: HttpErrorResponse) => {
        console.error('Could not remove patient from caregiver with error: ' + error.message);
      }
    )
  }

}
