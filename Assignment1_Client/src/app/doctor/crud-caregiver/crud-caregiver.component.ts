import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-crud-caregiver',
  templateUrl: './crud-caregiver.component.html',
  styleUrls: ['./crud-caregiver.component.css']
})
export class CrudCaregiverComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router) { }

  addCaregiver() {
    this.router.navigate(['addCaregiver'], {relativeTo: this.route});
  }

  modifyCaregiver() {
    this.router.navigate(['modifyCaregiver'], {relativeTo: this.route});
  }

  addPatient(){
    this.router.navigate(['addPatientCaregiver'], {relativeTo: this.route});
  }

  deletePatient(){
    this.router.navigate(['deletePatientCaregiver'], {relativeTo: this.route});
  }

  ngOnInit() {
  }

}
