import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyCaregiverComponent } from './modify-caregiver.component';

describe('ModifyCaregiverComponent', () => {
  let component: ModifyCaregiverComponent;
  let fixture: ComponentFixture<ModifyCaregiverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyCaregiverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyCaregiverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
