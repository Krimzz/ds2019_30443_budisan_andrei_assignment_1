import { Component, OnInit } from '@angular/core';
import { CaregiverInterface } from 'src/app/shared/models/interfaces/caregiver';
import { ApiService } from 'src/app/shared/services/api.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-modify-caregiver',
  templateUrl: './modify-caregiver.component.html',
  styleUrls: ['./modify-caregiver.component.css']
})
export class ModifyCaregiverComponent implements OnInit {

  caregivers: Array<CaregiverInterface>;

  constructor(private apiService: ApiService) {}

  ngOnInit() {
    this.getCaregivers();
  }

  getCaregivers() {
    this.apiService.getCaregivers().subscribe(
      (data: Array<CaregiverInterface>) => (this.caregivers = data),
      (error: HttpErrorResponse) => {
        console.error(  'Could not get caregivers with error: ' + error.message);
      }
    );
  }

  onRemove(caregiver: CaregiverInterface) {
    this.apiService.deleteCaregiver(caregiver.username).subscribe(
      () => {
        const index = this.caregivers.indexOf(caregiver, 0);
        if (index > -1) {
          this.caregivers.splice(index, 1);
        }
        console.log('Caregiver was removed!');
      },
      (error: HttpErrorResponse) => {
        console.error('Could not remove caregiver with error: ' + error.message);
      }
    );
  }

  onSave(caregiver: CaregiverInterface) {
    this.apiService.saveCaregiverDetails(caregiver).subscribe(
      (data: CaregiverInterface) => {
        const index = this.caregivers.indexOf(caregiver);
        if (index !== -1) {
          this.caregivers[index] = data;
        }
        console.log('Caregiver was updated!');
      },
      (error: HttpErrorResponse) => {
        console.error('Could not update caregiver with error: ' + error.message);
      },
    );
  }

  changeValue(id: number, property: string, event: any) {
    this.caregivers[id][property] = event.target.textContent;
  }
}
