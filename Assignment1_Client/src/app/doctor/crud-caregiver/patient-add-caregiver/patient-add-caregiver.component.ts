import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../shared/services/api.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-patient-add-caregiver',
  templateUrl: './patient-add-caregiver.component.html',
  styleUrls: ['./patient-add-caregiver.component.css']
})
export class PatientAddCaregiverComponent implements OnInit {
  patient: string;
  caregiver: string;
  constructor(private apiService: ApiService) { }

  ngOnInit() {
  }

  addPatientToCaregiver(){
    this.apiService.addPatientToCaregiver(this.patient,this.caregiver).subscribe(
      () => {
        console.log('Patient added to caregiver!');
      },
      (error: HttpErrorResponse) => {
        console.error('Could not add patient to caregiver with error: ' + error.message);
      }
    )
  }

}
