import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientAddCaregiverComponent } from './patient-add-caregiver.component';

describe('PatientAddCaregiverComponent', () => {
  let component: PatientAddCaregiverComponent;
  let fixture: ComponentFixture<PatientAddCaregiverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientAddCaregiverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientAddCaregiverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
