import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {AuthService} from '../shared/services/auth.service';
import {StorageService} from '../shared/services/storage.service';

@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.css']
})
export class DoctorComponent implements OnInit {

  updateDoctors() {
    this.router.navigate(['doctorsCRUD'], {relativeTo: this.route});
  }

  updateCaregivers() {
    this.router.navigate(['caregiverCRUD'], {relativeTo: this.route});
  }

  updatePatient() {
    this.router.navigate(['patientCRUD'], {relativeTo: this.route});
  }

  updateMedicalPlan() {
    this.router.navigate(['medicalPlanCRUD'], {relativeTo: this.route});
  }

  constructor(private route: ActivatedRoute, private router: Router, private storageService: StorageService) { }

  ngOnInit() {
  }

  signOut() {
    this.storageService.remove(this.storageService.app_token);
    this.storageService.remove(this.storageService.role_token);
    /** and the user gets redirected to the /login page */
    this.router.navigateByUrl('/login');
  }
}
