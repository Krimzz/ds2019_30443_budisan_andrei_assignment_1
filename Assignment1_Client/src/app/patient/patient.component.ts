import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { StorageService } from '../shared/services/storage.service';
import { MedicationPlanInterface } from '../shared/models/interfaces/medicationPlan';
import { DrugInterface } from '../shared/models/interfaces/drug';
import {ApiService} from '../shared/services/api.service';
import {HttpErrorResponse} from '@angular/common/http';
import {PatientInterface} from '../shared/models/interfaces/patient';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {

  constructor(private route: ActivatedRoute,private storageService: StorageService,private router: Router, private apiService: ApiService) {
  }

  ngOnInit() {
  }


  signOut() {
    this.storageService.remove(this.storageService.app_token);
    this.storageService.remove(this.storageService.role_token);
    /** and the user gets redirected to the /login page */
    this.router.navigateByUrl('/login');
  }

  viewAccount(){
    this.router.navigate(['viewAccountPatient'], {relativeTo: this.route});
  }

  viewMedicalPlan(){
    this.router.navigate(['showDrugsPatient'], {relativeTo: this.route});
  }

}
