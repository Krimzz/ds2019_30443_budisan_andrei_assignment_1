import {Component, Input, OnInit} from '@angular/core';
import { DrugInterface } from 'src/app/shared/models/interfaces/drug';
import {StorageService} from '../../../shared/services/storage.service';
import {PatientInterface} from '../../../shared/models/interfaces/patient';
import {HttpErrorResponse} from '@angular/common/http';
import {ApiService} from '../../../shared/services/api.service';

@Component({
  selector: 'app-show-drugs-plan',
  templateUrl: './show-drugs-plan.component.html',
  styleUrls: ['./show-drugs-plan.component.css']
})
export class ShowDrugsPlanComponent implements OnInit {

  drugs: Array<DrugInterface>;

  @Input()
  medicalPlanId: number;

  constructor(private storageService: StorageService, private apiService: ApiService) {
  }

  ngOnInit() {
    const loggedInUsername = this.storageService.get(this.storageService.loggedInUsername);
    this.apiService.getSelfPatientDetails(loggedInUsername).subscribe(
      (data: PatientInterface) => {
        for (let medicationPlan of data.medicationPlans) {
          if( medicationPlan.id == this.medicalPlanId) {
            this.drugs = medicationPlan.drugs;
          }
        }
        console.log('Got drugs from medication plans');
      },
      (error: HttpErrorResponse) => {
        console.error('Could not get medication plans: ' + error.message);
      }
    )
  }

}
