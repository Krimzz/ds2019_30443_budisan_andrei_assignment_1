import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowDrugsPlanComponent } from './show-drugs-plan.component';

describe('ShowDrugsPlanComponent', () => {
  let component: ShowDrugsPlanComponent;
  let fixture: ComponentFixture<ShowDrugsPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowDrugsPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowDrugsPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
