import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowDrugsPatientComponent } from './show-drugs-patient.component';

describe('ShowDrugsPatientComponent', () => {
  let component: ShowDrugsPatientComponent;
  let fixture: ComponentFixture<ShowDrugsPatientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowDrugsPatientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowDrugsPatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
