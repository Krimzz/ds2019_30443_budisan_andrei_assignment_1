import { Component, OnInit } from '@angular/core';
import { DrugInterface } from 'src/app/shared/models/interfaces/drug';
import {StorageService} from '../../shared/services/storage.service';
import {ApiService} from '../../shared/services/api.service';
import {HttpErrorResponse} from '@angular/common/http';
import {PatientInterface} from '../../shared/models/interfaces/patient';
import { MedicationPlanInterface } from 'src/app/shared/models/interfaces/medicationPlan';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-show-drugs-patient',
  templateUrl: './show-drugs-patient.component.html',
  styleUrls: ['./show-drugs-patient.component.css']
})
export class ShowDrugsPatientComponent implements OnInit {

  medicationPlans: Array<MedicationPlanInterface>;

  show = false;

  constructor(private route: ActivatedRoute,private storageService: StorageService,private router: Router, private apiService: ApiService) {
  }

  ngOnInit() {
    this.show = false;
    const loggedInUsername = this.storageService.get(this.storageService.loggedInUsername);
    this.apiService.getSelfPatientDetails(loggedInUsername).subscribe(
      (data: PatientInterface) => {
        this.medicationPlans = data.medicationPlans;
        console.log('Got medication plans from patient');
      },
      (error: HttpErrorResponse) => {
        console.error('Could not get medication plans: ' + error.message);
      }
    )
  }

  showDrugs(){
    this.show = true;
    this.router.navigate(['showDrugsPatient'], {relativeTo: this.route});
  }
}
