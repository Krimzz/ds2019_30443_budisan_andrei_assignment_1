import { Component, OnInit } from '@angular/core';
import {PatientInterface} from '../../shared/models/interfaces/patient';
import {ApiService} from '../../shared/services/api.service';
import {StorageService} from '../../shared/services/storage.service';
import {HttpErrorResponse} from '@angular/common/http';
import {MedicationPlanInterface} from '../../shared/models/interfaces/medicationPlan';

@Component({
  selector: 'app-view-account-patient',
  templateUrl: './view-account-patient.component.html',
  styleUrls: ['./view-account-patient.component.css']
})
export class ViewAccountPatientComponent implements OnInit {

  patient: PatientInterface;

  constructor(private apiService: ApiService, private storageService: StorageService) {
    const patientUsername = this.storageService.get(this.storageService.loggedInUsername);
    this.patient = {
      username: patientUsername,
      password: "",
      name: "",
      address: "",
      gender: "",
      birthDate: "",
      medicationPlans: new Array<MedicationPlanInterface>()
    }
  }

  ngOnInit() {
    const patientUsername = this.storageService.get(this.storageService.loggedInUsername);
    this.apiService.getSelfPatientDetails(patientUsername).subscribe(
      (data: PatientInterface) => (this.patient = data),
      (error: HttpErrorResponse) => {
        console.error('Could not get patient with error: ' + error.message);
      }
    );
  }

}
