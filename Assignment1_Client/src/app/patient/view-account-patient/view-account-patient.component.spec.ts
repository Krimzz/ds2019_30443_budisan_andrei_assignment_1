import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAccountPatientComponent } from './view-account-patient.component';

describe('ViewAccountPatientComponent', () => {
  let component: ViewAccountPatientComponent;
  let fixture: ComponentFixture<ViewAccountPatientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAccountPatientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAccountPatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
