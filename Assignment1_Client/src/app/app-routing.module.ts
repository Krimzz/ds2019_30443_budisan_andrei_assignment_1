import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DoctorComponent } from './doctor/doctor.component';
import { CaregiverComponent } from './caregiver/caregiver.component';
import { PatientComponent } from './patient/patient.component';
import { LoginComponent } from './login/login.component';
import { CrudDoctorsComponent } from './doctor/crud-doctors/crud-doctors.component';
import { AddDoctorsComponent } from './doctor/crud-doctors/add-doctors/add-doctors.component';
import { ModifyDoctorsComponent } from './doctor/crud-doctors/modify-doctors/modify-doctors.component';
import { AuthGuardDoctor} from './shared/services/authDoctor.guard.service';
import { AuthGuardPatient} from './shared/services/authPatient.guard.service';
import { AuthGuardCaregiver} from './shared/services/authCaregiver.guard.service';
import { CrudCaregiverComponent } from './doctor/crud-caregiver/crud-caregiver.component';
import { AddCaregiverComponent } from './doctor/crud-caregiver/add-caregiver/add-caregiver.component';
import { ModifyCaregiverComponent } from './doctor/crud-caregiver/modify-caregiver/modify-caregiver.component';
import { CrudPatientComponent } from './doctor/crud-patient/crud-patient.component';
import { CrudMedicationPlanComponent } from './doctor/crud-medication-plan/crud-medication-plan.component';
import { AddPatientComponent } from './doctor/crud-patient/add-patient/add-patient.component';
import { ModifyPatientComponent } from './doctor/crud-patient/modify-patient/modify-patient.component';
import { AddMedicationPlanComponent } from './doctor/crud-medication-plan/add-medication-plan/add-medication-plan.component';
import { ModifyMedicationPlanComponent } from './doctor/crud-medication-plan/modify-medication-plan/modify-medication-plan.component';
import { PatientAddCaregiverComponent } from './doctor/crud-caregiver/patient-add-caregiver/patient-add-caregiver.component';
import { PatientDeleteCaregiverComponent } from './doctor/crud-caregiver/patient-delete-caregiver/patient-delete-caregiver.component';
import { ViewAccountPatientComponent } from './patient/view-account-patient/view-account-patient.component';
import { ModifyDrugsComponent } from './doctor/crud-medication-plan/modify-medication-plan/modify-drugs/modify-drugs.component';
import { AddDrugsComponent } from './doctor/crud-medication-plan/modify-medication-plan/add-drugs/add-drugs.component';
import { ShowDrugsPatientComponent } from './patient/show-drugs-patient/show-drugs-patient.component';
import { ShowDrugsPlanComponent } from './patient/show-drugs-patient/show-drugs-plan/show-drugs-plan.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent},
  {
    path: 'doctor',
    component: DoctorComponent,
    children: [

      {
        path: 'doctorsCRUD',
        component: CrudDoctorsComponent,
        children: [
          {
            path: 'addDoctor',
            component: AddDoctorsComponent
          },
          {
            path: 'modifyDoctor',
            component: ModifyDoctorsComponent,

          },
        ]
      },

      {
        path: 'caregiverCRUD',
        component: CrudCaregiverComponent,
        children: [
          {
            path: 'addCaregiver',
            component: AddCaregiverComponent
          },
          {
            path: 'modifyCaregiver',
            component: ModifyCaregiverComponent
          },
          {
            path: 'addPatientCaregiver',
            component: PatientAddCaregiverComponent
          },
          {
            path: 'deletePatientCaregiver',
            component: PatientDeleteCaregiverComponent
          }
        ]
      },

      {
        path: 'patientCRUD',
        component: CrudPatientComponent,
        children: [
          {
            path: 'addPatient',
            component: AddPatientComponent
          },
          {
            path: 'modifyPatient',
            component: ModifyPatientComponent
          }
        ]
      },

      {
        path: 'medicalPlanCRUD',
        component: CrudMedicationPlanComponent,
        children: [
          {
            path: 'addMedicinePlan',
            component:  AddMedicationPlanComponent
          },
          {
            path: 'modifyMedicinePlan',
            component:  ModifyMedicationPlanComponent,
            children: [
              {
                path: 'viewDrugs',
                component:ModifyDrugsComponent
              },
              {
                path: 'showDrugs',
                component:AddDrugsComponent
              }
            ]
          }
        ]
      }
    ],
    canActivate: [AuthGuardDoctor]
  },

  {
    path: 'caregiver',
    component: CaregiverComponent,
    canActivate: [AuthGuardCaregiver]},
  {
    path: 'patient',
    component: PatientComponent,
    children: [
      {
        path:'showDrugsPatient',
        component: ShowDrugsPatientComponent,
        children: [
          {
            path: 'showDrugsPatient',
            component: ShowDrugsPlanComponent
          }
        ]
      },
      {
        path:'viewAccountPatient',
        component: ViewAccountPatientComponent
      }
    ],
    canActivate: [AuthGuardPatient]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
