import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, empty } from 'rxjs';
import { catchError } from 'rxjs/operators';
import {StorageService} from '../services/storage.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private storageService: StorageService, private router: Router) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authCode: string = this.storageService.get(this.storageService.app_token);

    let authReq = req.clone();

    if(!authReq.url.endsWith('/login')) {
      authReq = req.clone({
        setHeaders: {Authorization: `Basic ${authCode}`}
      });
    }

    return next.handle(authReq).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error instanceof HttpErrorResponse) {
          if (error.status === 401 || error.status === 403 || !error.status) {
            this.storageService.remove(this.storageService.app_token);
            this.router.navigateByUrl('/login', {
              queryParams: {
                unauthorized: true
              }
            });
            return empty();
          } else {
            return throwError(error);
          }
        }
      })
    );
  }
}
