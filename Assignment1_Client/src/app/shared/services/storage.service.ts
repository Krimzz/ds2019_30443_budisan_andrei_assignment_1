import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {
  public app_key = 'banking-service';

  public app_token = `${this.app_key}_token`;
  public role_token = 'role';
  public loggedInUsername;
  public medicationPlanId;

  public get(key: string): string {
    const sessionValue = localStorage.getItem(key);
    return sessionValue;
  }

  public set(key: string, value: string): string | false {
    localStorage.setItem(key, value);
    try {
      return this.get(key);
    } catch (error) {
      return false;
    }
  }

  public remove(key: string): boolean {
    localStorage.removeItem(key);
    return !this.get(key) ? true : false;
  }
}
