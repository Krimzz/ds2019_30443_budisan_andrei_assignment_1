import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import {StorageService} from './storage.service';

@Injectable()
export class AuthService {
  /** API Common URL for all end points */
  public BASE_URL = 'http://localhost:8080/medicalplatform';

  constructor(private http: HttpClient, private storageService: StorageService) {}

  isLoggedIn(): boolean {
    const token: any = this.storageService.get(this.storageService.app_token);
    return !!token;
  }

  login(username: string, password: string, authorizationCode: string): Observable<any> {
    const url = `${this.BASE_URL}/login`;
    const header: any = new HttpHeaders({
      Authorization: `Basic ${authorizationCode}`
    });

    return this.http.get(url, {
      headers: header
    });
  }
}
