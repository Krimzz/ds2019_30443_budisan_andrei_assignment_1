import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { AuthService } from './auth.service';
import {StorageService} from './storage.service';

@Injectable()
export class AuthGuardPatient implements CanActivate {

  constructor(private authService: AuthService, private router: Router, private  storageService: StorageService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (this.storageService.get(this.storageService.role_token) !== 'ROLE_Patient') {
      this.router.navigateByUrl('/login');
      return false;
    }
    return true;
  }
}
