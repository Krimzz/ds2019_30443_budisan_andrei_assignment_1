import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DoctorInterface} from '../models/interfaces/doctor';
import {PatientInterface} from '../models/interfaces/patient';
import {CaregiverInterface} from '../models/interfaces/caregiver';
import {MedicationPlanInterface} from '../models/interfaces/medicationPlan';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public BASE_URL = 'http://localhost:8080/medicalplatform';

  constructor(private http: HttpClient) {
  }

  //////////// Doctor /////////////

  public getDoctors(): Observable<Array<DoctorInterface>> {
    const url = `${this.BASE_URL}/doctor`;
    return this.http.get<Array<DoctorInterface>>(url);
  }

  public getDoctorDetails(id: string): Observable<DoctorInterface> {
    const url = `${this.BASE_URL}/doctor/${id}`;
    return this.http.get<DoctorInterface>(url);
  }

  public saveDoctorDetails(doctorData: DoctorInterface): Observable<DoctorInterface> {
    const url = `${this.BASE_URL}/doctor`;
    return this.http.put<DoctorInterface>(url, doctorData);
  }

  public deleteDoctor(id: string): Observable<any> {
    const url = `${this.BASE_URL}/doctor/${id}`;
    return this.http.delete<DoctorInterface>(url);
  }

  public insertDoctor(doctorData: DoctorInterface): Observable<DoctorInterface> {
    const url = `${this.BASE_URL}/doctor`;
    return this.http.post<DoctorInterface>(url, doctorData);
  }

  //////////// Patient /////////////

  public getPatients(): Observable<Array<PatientInterface>> {
    const url = `${this.BASE_URL}/doctor/patient`;
    return this.http.get<Array<PatientInterface>>(url);
  }

  public getPatientDetails(id: string): Observable<PatientInterface> {
    const url = `${this.BASE_URL}/doctor/patient/${id}`;
    return this.http.get<PatientInterface>(url);
  }

  public getSelfPatientDetails(id: string): Observable<PatientInterface> {
    const url = `${this.BASE_URL}/patient/${id}`;
    return this.http.get<PatientInterface>(url);
  }

  public savePatientDetails(patientData: PatientInterface): Observable<PatientInterface> {
    const url = `${this.BASE_URL}/doctor/patient`;
    return this.http.put<PatientInterface>(url, patientData);
  }

  public deletePatient(id: string): Observable<any> {
    const url = `${this.BASE_URL}/doctor/patient/${id}`;
    return this.http.delete<PatientInterface>(url);
  }

  public insertPatient(patientData: PatientInterface): Observable<PatientInterface> {
    const url = `${this.BASE_URL}/doctor/patient`;
    return this.http.post<PatientInterface>(url, patientData);
  }

  //////////// Caregiver /////////////

  public getCaregivers(): Observable<Array<CaregiverInterface>> {
    const url = `${this.BASE_URL}/doctor/caregiver`;
    return this.http.get<Array<CaregiverInterface>>(url);
  }

  public getSelfCaregiverDetails(id: string): Observable<CaregiverInterface> {
    const url = `${this.BASE_URL}/caregiver/${id}`;
    return this.http.get<CaregiverInterface>(url);
  }

  public getCaregiverDetails(id: string): Observable<CaregiverInterface> {
    const url = `${this.BASE_URL}/doctor/caregiver/${id}`;
    return this.http.get<CaregiverInterface>(url);
  }

  public saveCaregiverDetails(caregiverData: CaregiverInterface): Observable<CaregiverInterface> {
    const url = `${this.BASE_URL}/doctor/caregiver`;
    return this.http.put<CaregiverInterface>(url, caregiverData);
  }

  public deleteCaregiver(id: string): Observable<any> {
    const url = `${this.BASE_URL}/doctor/caregiver/${id}`;
    return this.http.delete<CaregiverInterface>(url);
  }

  public insertCaregiver(caregiverData: CaregiverInterface): Observable<CaregiverInterface> {
    const url = `${this.BASE_URL}/doctor/caregiver`;
    return this.http.post<CaregiverInterface>(url, caregiverData);
  }

  public addPatientToCaregiver(patientUsername: string, caregiverUsername: string): Observable<any> {
    const url = `${this.BASE_URL}/doctor/caregiver/add/${patientUsername}/${caregiverUsername}`;
    return this.http.put<any>(url, null);
  }

  public removePatientFromCaregiver(patientUsername: string, caregiverUsername: string): Observable<any> {
    const url = `${this.BASE_URL}/doctor/caregiver/remove/${patientUsername}/${caregiverUsername}`;
    return this.http.put<any>(url, null);
  }

  //////////// MedicalPlan /////////////

  public getMedicationPlans(): Observable<Array<MedicationPlanInterface>> {
    const url = `${this.BASE_URL}/doctor/medicationplan`;
    return this.http.get<Array<MedicationPlanInterface>>(url);
  }

  public getMedicationPlanDetails(id: number): Observable<MedicationPlanInterface> {
    const url = `${this.BASE_URL}/doctor/medicationplan/${id}`;
    return this.http.get<MedicationPlanInterface>(url);
  }

  public saveMedicationPlanDetails(medicationPlanData: MedicationPlanInterface): Observable<MedicationPlanInterface> {
    const url = `${this.BASE_URL}/doctor/medicationplan`;
    return this.http.put<MedicationPlanInterface>(url, medicationPlanData);
  }

  public deleteMedicationPlan(id: number): Observable<any> {
    const url = `${this.BASE_URL}/doctor/medicationplan/${id}`;
    return this.http.delete<MedicationPlanInterface>(url);
  }

  public insertMedicationPlan(medicationPlanData: MedicationPlanInterface): Observable<MedicationPlanInterface> {
    const url = `${this.BASE_URL}/doctor/medicationplan`;
    return this.http.post<MedicationPlanInterface>(url, medicationPlanData);
  }

  public addMedicationPlanToPatient(doctorId: string, patientId: string, medicationPlanData: MedicationPlanInterface): Observable<MedicationPlanInterface> {
    const url = `${this.BASE_URL}/doctor/medicationplan/${doctorId}/${patientId}`;
    return this.http.post<MedicationPlanInterface>(url, medicationPlanData);
  }
}
