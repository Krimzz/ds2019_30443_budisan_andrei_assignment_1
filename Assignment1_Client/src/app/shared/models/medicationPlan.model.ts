import {Injectable} from '@angular/core';
import {MedicationPlanInterface} from './interfaces/medicationPlan';

@Injectable()
export class MedicationPlanModel {
  all: Array<MedicationPlanInterface>;

  selectedMedicationPlanId: number;

  setSelected(id: number) {
    return this.all.find((medicationPlan: MedicationPlanInterface) => medicationPlan.id === id);
  }

  removeSelected() {
    this.selectedMedicationPlanId = undefined;
  }

  clear() {
    this.removeSelected();
    this.all = undefined;
  }
}
