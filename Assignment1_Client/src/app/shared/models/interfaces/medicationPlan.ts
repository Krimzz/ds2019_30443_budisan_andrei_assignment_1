import {DrugInterface} from './drug';

export interface MedicationPlanInterface {
  id: number;
  startDate: string;
  endDate: string;
  drugs: Array<DrugInterface>;
}
