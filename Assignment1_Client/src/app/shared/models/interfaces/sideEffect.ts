
export interface SideEffectInterface {
  id: number;
  description: string;
}
