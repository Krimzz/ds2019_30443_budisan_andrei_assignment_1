import {PatientInterface} from './patient';

export interface CaregiverInterface {
  username: string;
  password: string;
  name: string;
  address: string;
  gender: string;
  birthDate: string;
  patients: Array<PatientInterface>;
}
