import {MedicationPlanInterface} from './medicationPlan';

export interface DoctorInterface {
  username: string;
  password: string;
  name: string;
  address: string;
  gender: string;
  birthDate: string;
  domain: string;
  medicationPlans: Array<MedicationPlanInterface>;
}
