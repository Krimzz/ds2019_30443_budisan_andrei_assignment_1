import {SideEffectInterface} from './sideEffect';

export interface DrugInterface {
  id: number;
  name: string;
  intakeStartTime: string;
  intakeEndTime: string;
  dosage: number;
  sideEffects: Array<SideEffectInterface>;
}
