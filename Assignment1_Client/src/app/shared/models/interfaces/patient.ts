import {MedicationPlanInterface} from './medicationPlan';

export interface PatientInterface {
  username: string;
  password: string;
  name: string;
  address: string;
  gender: string;
  birthDate: string;
  medicationPlans: Array<MedicationPlanInterface>;
}
