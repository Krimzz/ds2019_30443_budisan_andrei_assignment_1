import {Injectable} from '@angular/core';
import {CaregiverInterface} from './interfaces/caregiver';

@Injectable()
export class CaregiverModel {
  all: Array<CaregiverInterface>;

  selectedCaregiverUsername: string;

  setSelected(username: string) {
    return this.all.find((caregiver: CaregiverInterface) => caregiver.username === username);
  }

  removeSelected() {
    this.selectedCaregiverUsername = undefined;
  }

  clear() {
    this.removeSelected();
    this.all = undefined;
  }
}
