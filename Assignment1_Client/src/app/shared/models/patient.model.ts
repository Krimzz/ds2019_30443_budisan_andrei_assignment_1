import {Injectable} from '@angular/core';
import {PatientInterface} from './interfaces/patient';

@Injectable()
export class PatientModel {
  all: Array<PatientInterface>;

  selectedPatientUsername: string;

  setSelected(username: string) {
    return this.all.find((patient: PatientInterface) => patient.username === username);
  }

  removeSelected() {
    this.selectedPatientUsername = undefined;
  }

  clear() {
    this.removeSelected();
    this.all = undefined;
  }
}
