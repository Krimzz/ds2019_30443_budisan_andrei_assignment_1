import {Injectable} from '@angular/core';
import {DrugInterface} from './interfaces/drug';

@Injectable()
export class DrugModel {
  all: Array<DrugInterface>;

  selectedDrugId: number;

  setSelected(id: number) {
    return this.all.find((drug: DrugInterface) => drug.id === id);
  }

  removeSelected() {
    this.selectedDrugId = undefined;
  }

  clear() {
    this.removeSelected();
    this.all = undefined;
  }
}
