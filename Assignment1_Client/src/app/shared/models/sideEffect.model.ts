import {Injectable} from '@angular/core';
import {SideEffectInterface} from './interfaces/sideEffect';

@Injectable()
export class SideEffectModel {
  all: Array<SideEffectInterface>;

  selectedSideEffectId: number;

  setSelected(id: number) {
    return this.all.find((sideEffect: SideEffectInterface) => sideEffect.id === id);
  }

  removeSelected() {
    this.selectedSideEffectId = undefined;
  }

  clear() {
    this.removeSelected();
    this.all = undefined;
  }
}
