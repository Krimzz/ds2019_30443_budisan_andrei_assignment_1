import {Injectable} from '@angular/core';
import {DoctorInterface} from './interfaces/doctor';

@Injectable()
export class DoctorModel {
  all: Array<DoctorInterface>;

  selectedDoctorUsername: string;

  setSelected(username: string) {
    return this.all.find((doctor: DoctorInterface) => doctor.username === username);
  }

  removeSelected() {
    this.selectedDoctorUsername = undefined;
  }

  clear() {
    this.removeSelected();
    this.all = undefined;
  }
}
