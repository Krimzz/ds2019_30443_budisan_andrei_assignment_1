import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DoctorComponent } from './doctor/doctor.component';
import { PatientComponent } from './patient/patient.component';
import { CaregiverComponent } from './caregiver/caregiver.component';
import { LoginComponent } from './login/login.component';
import { CrudDoctorsComponent } from './doctor/crud-doctors/crud-doctors.component';
import { AddDoctorsComponent } from './doctor/crud-doctors/add-doctors/add-doctors.component';
import { ModifyDoctorsComponent } from './doctor/crud-doctors/modify-doctors/modify-doctors.component';

import { FormsModule } from '@angular/forms';
import {AuthService} from './shared/services/auth.service';
import {StorageService} from './shared/services/storage.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthInterceptor} from './shared/interceptor/http.interceptor';
import {ApiService} from './shared/services/api.service';
import {AuthGuardDoctor} from './shared/services/authDoctor.guard.service';
import {AuthGuardPatient} from './shared/services/authPatient.guard.service';
import {AuthGuardCaregiver} from './shared/services/authCaregiver.guard.service';
import { CrudCaregiverComponent } from './doctor/crud-caregiver/crud-caregiver.component';
import { AddCaregiverComponent } from './doctor/crud-caregiver/add-caregiver/add-caregiver.component';
import { ModifyCaregiverComponent } from './doctor/crud-caregiver/modify-caregiver/modify-caregiver.component';
import { CrudPatientComponent } from './doctor/crud-patient/crud-patient.component';
import { CrudMedicationPlanComponent } from './doctor/crud-medication-plan/crud-medication-plan.component';
import { AddPatientComponent } from './doctor/crud-patient/add-patient/add-patient.component';
import { ModifyPatientComponent } from './doctor/crud-patient/modify-patient/modify-patient.component';
import { AddMedicationPlanComponent } from './doctor/crud-medication-plan/add-medication-plan/add-medication-plan.component';
import { ModifyMedicationPlanComponent } from './doctor/crud-medication-plan/modify-medication-plan/modify-medication-plan.component';
import { PatientAddCaregiverComponent } from './doctor/crud-caregiver/patient-add-caregiver/patient-add-caregiver.component';
import { PatientDeleteCaregiverComponent } from './doctor/crud-caregiver/patient-delete-caregiver/patient-delete-caregiver.component';
import { ViewAccountPatientComponent } from './patient/view-account-patient/view-account-patient.component';
import { ModifyDrugsComponent } from './doctor/crud-medication-plan/modify-medication-plan/modify-drugs/modify-drugs.component';
import { AddDrugsComponent } from './doctor/crud-medication-plan/modify-medication-plan/add-drugs/add-drugs.component';
import { ShowDrugsPatientComponent } from './patient/show-drugs-patient/show-drugs-patient.component';
import { ShowDrugsPlanComponent } from './patient/show-drugs-patient/show-drugs-plan/show-drugs-plan.component';


@NgModule({
  declarations: [
    AppComponent,
    DoctorComponent,
    PatientComponent,
    CaregiverComponent,
    LoginComponent,
    CrudDoctorsComponent,
    AddDoctorsComponent,
    ModifyDoctorsComponent,
    CrudCaregiverComponent,
    AddCaregiverComponent,
    ModifyCaregiverComponent,
    CrudPatientComponent,
    CrudMedicationPlanComponent,
    AddPatientComponent,
    ModifyPatientComponent,
    AddMedicationPlanComponent,
    ModifyMedicationPlanComponent,
    PatientAddCaregiverComponent,
    PatientDeleteCaregiverComponent,
    ViewAccountPatientComponent,
    ModifyDrugsComponent,
    AddDrugsComponent,
    ShowDrugsPatientComponent,
    ShowDrugsPlanComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    AuthService,
    AuthGuardDoctor,
    AuthGuardPatient,
    AuthGuardCaregiver,
    StorageService,
    ApiService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
