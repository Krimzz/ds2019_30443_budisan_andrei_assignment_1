import { Component, OnInit } from '@angular/core';
import { StorageService } from '../shared/services/storage.service';
import { Router, ActivatedRoute } from '@angular/router';
import {PatientInterface} from '../shared/models/interfaces/patient';
import {CaregiverInterface} from '../shared/models/interfaces/caregiver';
import {HttpErrorResponse} from '@angular/common/http';
import {ApiService} from '../shared/services/api.service';

@Component({
  selector: 'app-caregiver',
  templateUrl: './caregiver.component.html',
  styleUrls: ['./caregiver.component.css']
})
export class CaregiverComponent implements OnInit {

  patients: Array<PatientInterface>;

  constructor(private route: ActivatedRoute,private storageService: StorageService,private router: Router, private apiService: ApiService) { }

  ngOnInit() {
    const caregiverUsername = this.storageService.get(this.storageService.loggedInUsername);
    this.apiService.getSelfCaregiverDetails(caregiverUsername).subscribe(
      (data: CaregiverInterface) => (
        this.patients = data.patients
      ),
      (error: HttpErrorResponse) => {
        console.error('Could not get patients with error: ' + error.message);
      }
    );
  }

  signOut() {
    this.storageService.remove(this.storageService.app_token);
    this.storageService.remove(this.storageService.role_token);
    /** and the user gets redirected to the /login page */
    this.router.navigateByUrl('/login');
  }
}
