import { Component, OnInit } from '@angular/core';
import {AuthService} from '../shared/services/auth.service';
import {StorageService} from '../shared/services/storage.service';
import {HttpErrorResponse} from '@angular/common/http';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-login-doctors',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public username: string;
  public password: string;
  error = '';

  subscription: Subscription;

  constructor(private authService: AuthService, private storageService: StorageService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {

    this.subscription = this.route.queryParams.subscribe((params: Params) => {
      if (params['unauthorized']) {
        this.error = 'Access denied';
      }
      if (this.authService.isLoggedIn() && !this.error) {
        this.router.navigate(['/login']);
      }
    });
  }

  login(username: string, password: string) {

    const authorizationCode: string = btoa(`${username}:${password}`);

    this.authService.login(username, password, authorizationCode).subscribe(
      (response: any) => {
        this.storageService.set(this.storageService.app_token, authorizationCode);
        this.storageService.set(this.storageService.role_token, response.authorities[0].authority);
        this.storageService.set(this.storageService.loggedInUsername, username);
        if (this.storageService.get(this.storageService.role_token) === 'ROLE_Doctor') {
          this.router.navigateByUrl('/doctor');
        }
        if (this.storageService.get(this.storageService.role_token) === 'ROLE_Patient') {
          this.router.navigateByUrl('/patient');
        }
        if (this.storageService.get(this.storageService.role_token) === 'ROLE_Caregiver') {
          this.router.navigateByUrl('/caregiver');
        }
      },
      (error: HttpErrorResponse) => {
        console.error('Login failed with error: ' + error.message);
      }
    );

  }
}
